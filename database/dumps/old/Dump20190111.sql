-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `PRODUCTS`
--

DROP TABLE IF EXISTS `PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PRODUCTS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `withdrawn` tinyint(1) DEFAULT '0',
  `origin` tinyint(1) NOT NULL,
  `photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PRODUCTS`
--

LOCK TABLES `PRODUCTS` WRITE;
/*!40000 ALTER TABLE `PRODUCTS` DISABLE KEYS */;
INSERT INTO `PRODUCTS` VALUES (1,'meat','nice meat','meat',0,1,NULL),(2,'meat','nice meat','meat',0,1,NULL),(3,'asdf','asdf','asdf',1,1,'asdf');
/*!40000 ALTER TABLE `PRODUCTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PRODUCTS_TAGS`
--

DROP TABLE IF EXISTS `PRODUCTS_TAGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PRODUCTS_TAGS` (
  `PRODUCTS_idProducts` int(11) NOT NULL,
  `PRODUCTS_TAGScol` varchar(45) NOT NULL,
  PRIMARY KEY (`PRODUCTS_idProducts`,`PRODUCTS_TAGScol`),
  KEY `fk_table1_PRODUCTS1_idx` (`PRODUCTS_idProducts`),
  CONSTRAINT `fk_table1_PRODUCTS1` FOREIGN KEY (`PRODUCTS_idProducts`) REFERENCES `PRODUCTS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PRODUCTS_TAGS`
--

LOCK TABLES `PRODUCTS_TAGS` WRITE;
/*!40000 ALTER TABLE `PRODUCTS_TAGS` DISABLE KEYS */;
INSERT INTO `PRODUCTS_TAGS` VALUES (1,'tag1'),(1,'tag2');
/*!40000 ALTER TABLE `PRODUCTS_TAGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `P_CATEGORIES`
--

DROP TABLE IF EXISTS `P_CATEGORIES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `P_CATEGORIES` (
  `idProductCategories` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idProductCategories`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `P_CATEGORIES`
--

LOCK TABLES `P_CATEGORIES` WRITE;
/*!40000 ALTER TABLE `P_CATEGORIES` DISABLE KEYS */;
/*!40000 ALTER TABLE `P_CATEGORIES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STORES`
--

DROP TABLE IF EXISTS `STORES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STORES` (
  `idStores` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `withdrawn` tinyint(1) NOT NULL DEFAULT '0',
  `typeOfStore` varchar(45) DEFAULT NULL,
  `registerDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `USERS_idUsers` int(11) NOT NULL,
  PRIMARY KEY (`idStores`,`USERS_idUsers`),
  KEY `fk_STORES_USERS_idx` (`USERS_idUsers`),
  CONSTRAINT `fk_STORES_USERS` FOREIGN KEY (`USERS_idUsers`) REFERENCES `USERS` (`idUsers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORES`
--

LOCK TABLES `STORES` WRITE;
/*!40000 ALTER TABLE `STORES` DISABLE KEYS */;
INSERT INTO `STORES` VALUES (1,'test','test add',33,32,0,'meat store','2018-12-29 04:01:49',1),(2,'test','test address',0,0,1,NULL,'2019-01-10 12:03:42',1),(3,'testign no tags','testing adafsdf',0,0,1,NULL,'2019-01-10 12:18:41',1);
/*!40000 ALTER TABLE `STORES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STORES_TAGS`
--

DROP TABLE IF EXISTS `STORES_TAGS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STORES_TAGS` (
  `STORES_idStores` int(11) NOT NULL,
  `STORES_USERS_idUsers` int(11) NOT NULL,
  `tag` varchar(45) NOT NULL,
  PRIMARY KEY (`tag`,`STORES_idStores`),
  KEY `fk_table1_STORES1_idx` (`STORES_idStores`,`STORES_USERS_idUsers`),
  CONSTRAINT `fk_table1_STORES1` FOREIGN KEY (`STORES_idStores`, `STORES_USERS_idUsers`) REFERENCES `STORES` (`idStores`, `USERS_idUsers`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORES_TAGS`
--

LOCK TABLES `STORES_TAGS` WRITE;
/*!40000 ALTER TABLE `STORES_TAGS` DISABLE KEYS */;
INSERT INTO `STORES_TAGS` VALUES (1,1,'tag1'),(1,1,'tag2'),(2,1,'hello there'),(2,1,'hello there 2');
/*!40000 ALTER TABLE `STORES_TAGS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STORES_has_PRODUCTS`
--

DROP TABLE IF EXISTS `STORES_has_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STORES_has_PRODUCTS` (
  `STORES_idStores` int(11) NOT NULL,
  `STORES_USERS_idUsers` int(11) NOT NULL,
  `PRODUCTS_idProducts` int(11) NOT NULL,
  PRIMARY KEY (`STORES_idStores`,`STORES_USERS_idUsers`,`PRODUCTS_idProducts`),
  KEY `fk_STORES_has_PRODUCTS_PRODUCTS1_idx` (`PRODUCTS_idProducts`),
  KEY `fk_STORES_has_PRODUCTS_STORES1_idx` (`STORES_idStores`,`STORES_USERS_idUsers`),
  CONSTRAINT `fk_STORES_has_PRODUCTS_PRODUCTS1` FOREIGN KEY (`PRODUCTS_idProducts`) REFERENCES `PRODUCTS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_STORES_has_PRODUCTS_STORES1` FOREIGN KEY (`STORES_idStores`, `STORES_USERS_idUsers`) REFERENCES `STORES` (`idStores`, `USERS_idUsers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORES_has_PRODUCTS`
--

LOCK TABLES `STORES_has_PRODUCTS` WRITE;
/*!40000 ALTER TABLE `STORES_has_PRODUCTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STORES_has_PRODUCTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STORES_has_USERS_has_PRODUCTS`
--

DROP TABLE IF EXISTS `STORES_has_USERS_has_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STORES_has_USERS_has_PRODUCTS` (
  `STORES_idStores` int(11) NOT NULL,
  `STORES_USERS_idUsers` int(11) NOT NULL,
  `USERS_has_PRODUCTS_USERS_idUsers` int(11) NOT NULL,
  `USERS_has_PRODUCTS_PRODUCTS_idProducts` int(11) NOT NULL,
  PRIMARY KEY (`STORES_idStores`,`STORES_USERS_idUsers`,`USERS_has_PRODUCTS_USERS_idUsers`,`USERS_has_PRODUCTS_PRODUCTS_idProducts`),
  KEY `fk_STORES_has_USERS_has_PRODUCTS_STORES1_idx` (`STORES_idStores`,`STORES_USERS_idUsers`),
  CONSTRAINT `fk_STORES_has_USERS_has_PRODUCTS_STORES1` FOREIGN KEY (`STORES_idStores`, `STORES_USERS_idUsers`) REFERENCES `STORES` (`idStores`, `USERS_idUsers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STORES_has_USERS_has_PRODUCTS`
--

LOCK TABLES `STORES_has_USERS_has_PRODUCTS` WRITE;
/*!40000 ALTER TABLE `STORES_has_USERS_has_PRODUCTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `STORES_has_USERS_has_PRODUCTS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USERS`
--

DROP TABLE IF EXISTS `USERS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USERS` (
  `idUsers` int(11) NOT NULL AUTO_INCREMENT,
  `Blocked` tinyint(1) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `password` mediumtext NOT NULL,
  `userName` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `avatar` varchar(45) DEFAULT NULL,
  `registerDate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idUsers`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USERS`
--

LOCK TABLES `USERS` WRITE;
/*!40000 ALTER TABLE `USERS` DISABLE KEYS */;
INSERT INTO `USERS` VALUES (1,0,1,'yolo','usrname','test@test',NULL,'2018-12-29 04:01:16');
/*!40000 ALTER TABLE `USERS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USERS_has_PRODUCTS`
--

DROP TABLE IF EXISTS `USERS_has_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USERS_has_PRODUCTS` (
  `USERS_idUsers` int(11) NOT NULL,
  `PRODUCTS_idProducts` int(11) NOT NULL,
  `price` varchar(45) DEFAULT NULL,
  `registerDate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`USERS_idUsers`,`PRODUCTS_idProducts`),
  KEY `fk_USERS_has_PRODUCTS_PRODUCTS1_idx` (`PRODUCTS_idProducts`),
  KEY `fk_USERS_has_PRODUCTS_USERS1_idx` (`USERS_idUsers`),
  CONSTRAINT `fk_USERS_has_PRODUCTS_PRODUCTS1` FOREIGN KEY (`PRODUCTS_idProducts`) REFERENCES `PRODUCTS` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_USERS_has_PRODUCTS_USERS1` FOREIGN KEY (`USERS_idUsers`) REFERENCES `USERS` (`idUsers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USERS_has_PRODUCTS`
--

LOCK TABLES `USERS_has_PRODUCTS` WRITE;
/*!40000 ALTER TABLE `USERS_has_PRODUCTS` DISABLE KEYS */;
/*!40000 ALTER TABLE `USERS_has_PRODUCTS` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-11 17:41:59
