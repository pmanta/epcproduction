-- MySQL Script generated by MySQL Workbench
-- Sat 29 Dec 2018 03:45:06 AM EET
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`USERS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`USERS` (
  `idUsers` INT NOT NULL AUTO_INCREMENT,
  `Blocked` TINYINT(1) NOT NULL,
  `isAdmin` TINYINT(1) NOT NULL,
  `password` MEDIUMTEXT NOT NULL,
  `userName` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `avatar` VARCHAR(45) NULL DEFAULT NULL,
  `registerDate` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idUsers`),
  UNIQUE INDEX `userName_UNIQUE` (`userName` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`STORES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`STORES` (
  `idStores` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `longitude` DOUBLE NOT NULL,
  `latitude` DOUBLE NOT NULL,
  `withdrawn` TINYINT(1) NOT NULL DEFAULT 0,
  `typeOfStore` VARCHAR(45) NULL DEFAULT NULL,
  `registerDate` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `USERS_idUsers` INT NOT NULL,
  PRIMARY KEY (`idStores`, `USERS_idUsers`),
  INDEX `fk_STORES_USERS_idx` (`USERS_idUsers` ASC),
  CONSTRAINT `fk_STORES_USERS`
    FOREIGN KEY (`USERS_idUsers`)
    REFERENCES `mydb`.`USERS` (`idUsers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`PRODUCTS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`PRODUCTS` (
  `idProducts` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `category` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NULL,
  `withdrawn` TINYINT(1) NULL DEFAULT 0,
  `origin` TINYINT(1) NOT NULL,
  `photo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idProducts`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`P_CATEGORIES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`P_CATEGORIES` (
  `idProductCategories` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `photo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idProductCategories`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`STORES_has_USERS_has_PRODUCTS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`STORES_has_USERS_has_PRODUCTS` (
  `STORES_idStores` INT NOT NULL,
  `STORES_USERS_idUsers` INT NOT NULL,
  `USERS_has_PRODUCTS_USERS_idUsers` INT NOT NULL,
  `USERS_has_PRODUCTS_PRODUCTS_idProducts` INT NOT NULL,
  PRIMARY KEY (`STORES_idStores`, `STORES_USERS_idUsers`, `USERS_has_PRODUCTS_USERS_idUsers`, `USERS_has_PRODUCTS_PRODUCTS_idProducts`),
  INDEX `fk_STORES_has_USERS_has_PRODUCTS_STORES1_idx` (`STORES_idStores` ASC, `STORES_USERS_idUsers` ASC),
  CONSTRAINT `fk_STORES_has_USERS_has_PRODUCTS_STORES1`
    FOREIGN KEY (`STORES_idStores` , `STORES_USERS_idUsers`)
    REFERENCES `mydb`.`STORES` (`idStores` , `USERS_idUsers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`STORES_has_PRODUCTS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`STORES_has_PRODUCTS` (
  `STORES_idStores` INT NOT NULL,
  `STORES_USERS_idUsers` INT NOT NULL,
  `PRODUCTS_idProducts` INT NOT NULL,
  PRIMARY KEY (`STORES_idStores`, `STORES_USERS_idUsers`, `PRODUCTS_idProducts`),
  INDEX `fk_STORES_has_PRODUCTS_PRODUCTS1_idx` (`PRODUCTS_idProducts` ASC),
  INDEX `fk_STORES_has_PRODUCTS_STORES1_idx` (`STORES_idStores` ASC, `STORES_USERS_idUsers` ASC),
  CONSTRAINT `fk_STORES_has_PRODUCTS_STORES1`
    FOREIGN KEY (`STORES_idStores` , `STORES_USERS_idUsers`)
    REFERENCES `mydb`.`STORES` (`idStores` , `USERS_idUsers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_STORES_has_PRODUCTS_PRODUCTS1`
    FOREIGN KEY (`PRODUCTS_idProducts`)
    REFERENCES `mydb`.`PRODUCTS` (`idProducts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`USERS_has_PRODUCTS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`USERS_has_PRODUCTS` (
  `USERS_idUsers` INT NOT NULL,
  `PRODUCTS_idProducts` INT NOT NULL,
  `price` VARCHAR(45) NULL DEFAULT NULL,
  `registerDate` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`USERS_idUsers`, `PRODUCTS_idProducts`),
  INDEX `fk_USERS_has_PRODUCTS_PRODUCTS1_idx` (`PRODUCTS_idProducts` ASC),
  INDEX `fk_USERS_has_PRODUCTS_USERS1_idx` (`USERS_idUsers` ASC),
  CONSTRAINT `fk_USERS_has_PRODUCTS_USERS1`
    FOREIGN KEY (`USERS_idUsers`)
    REFERENCES `mydb`.`USERS` (`idUsers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_USERS_has_PRODUCTS_PRODUCTS1`
    FOREIGN KEY (`PRODUCTS_idProducts`)
    REFERENCES `mydb`.`PRODUCTS` (`idProducts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`STORES_TAGS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`STORES_TAGS` (
  `STORES_idStores` INT NOT NULL,
  `STORES_USERS_idUsers` INT NOT NULL,
  `tag` VARCHAR(45) NOT NULL,
  INDEX `fk_table1_STORES1_idx` (`STORES_idStores` ASC, `STORES_USERS_idUsers` ASC),
  PRIMARY KEY (`tag`, `STORES_idStores`),
  CONSTRAINT `fk_table1_STORES1`
    FOREIGN KEY (`STORES_idStores` , `STORES_USERS_idUsers`)
    REFERENCES `mydb`.`STORES` (`idStores` , `USERS_idUsers`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`PRODUCTS_TAGS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`PRODUCTS_TAGS` (
  `PRODUCTS_idProducts` INT NOT NULL,
  `PRODUCTS_TAGScol` VARCHAR(45) NOT NULL,
  INDEX `fk_table1_PRODUCTS1_idx` (`PRODUCTS_idProducts` ASC),
  PRIMARY KEY (`PRODUCTS_idProducts`, `PRODUCTS_TAGScol`),
  CONSTRAINT `fk_table1_PRODUCTS1`
    FOREIGN KEY (`PRODUCTS_idProducts`)
    REFERENCES `mydb`.`PRODUCTS` (`idProducts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
