CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `p_categories`
--

LOCK TABLES `p_categories` WRITE;
/*!40000 ALTER TABLE `p_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'product1','desc1','cat1',0,0,NULL),(2,'product2','desc2','cat2',0,0,NULL),(3,'product3','desc3','cat3',1,1,NULL),(4,'product3','desc4','cat4',1,1,NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `products_tags`
--

LOCK TABLES `products_tags` WRITE;
/*!40000 ALTER TABLE `products_tags` DISABLE KEYS */;
INSERT INTO `products_tags` VALUES (1,'tag1'),(1,'tag2'),(2,'tag1'),(2,'tag2'),(3,'tag1');
/*!40000 ALTER TABLE `products_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (1,'test1','address1',33.434242,33.32423424,0,NULL,'2019-01-09 20:24:26',1),(2,'test2','address2',33.434242,33.32423424,0,NULL,'2019-01-09 20:24:55',1),(3,'test3','address3',33.434242,33.32423424,0,NULL,'2019-01-09 20:25:16',1);
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `stores_has_products`
--

LOCK TABLES `stores_has_products` WRITE;
/*!40000 ALTER TABLE `stores_has_products` DISABLE KEYS */;
INSERT INTO `stores_has_products` VALUES (1,1,'25','2019-01-09 20:23:39','2019-01-09 20:23:39'),(1,2,'13','2019-01-09 20:23:39','2019-01-09 20:23:39'),(2,1,'55','2019-01-09 20:23:39','2019-01-09 20:23:39'),(2,2,'43','2019-01-09 20:23:39','2019-01-09 20:23:39');
/*!40000 ALTER TABLE `stores_has_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `stores_has_users_has_products`
--

LOCK TABLES `stores_has_users_has_products` WRITE;
/*!40000 ALTER TABLE `stores_has_users_has_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `stores_has_users_has_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `stores_tags`
--

LOCK TABLES `stores_tags` WRITE;
/*!40000 ALTER TABLE `stores_tags` DISABLE KEYS */;
INSERT INTO `stores_tags` VALUES (1,1,'tag1'),(2,1,'tag1'),(3,1,'tag1'),(1,1,'tag2'),(2,1,'tag2'),(3,1,'tag2'),(1,1,'tag3'),(2,1,'tag3'),(3,1,'tag3'),(1,1,'tag4'),(2,1,'tag4'),(1,1,'tag5'),(2,1,'tag5'),(1,1,'tag6'),(2,1,'tag6'),(2,1,'tag7'),(2,1,'tag8');
/*!40000 ALTER TABLE `stores_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users`( `password`, `username`, `email`, `role`) VALUES ('awda2r2ra','test_admin','admin@adminl.gr','ROLE_ADMIN');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_has_products`
--

LOCK TABLES `users_has_products` WRITE;
/*!40000 ALTER TABLE `users_has_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_has_products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-12 18:03:31
