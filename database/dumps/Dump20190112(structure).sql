CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `p_categories`
--

DROP TABLE IF EXISTS `p_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `p_categories` (
  `idProductCategories` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idProductCategories`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `withdrawn` tinyint(1) DEFAULT '0',
  `origin` tinyint(1) NOT NULL,
  `photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products_tags`
--

DROP TABLE IF EXISTS `products_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `products_tags` (
  `PRODUCTS_idProducts` int(11) NOT NULL,
  `PRODUCTS_TAGScol` varchar(45) NOT NULL,
  PRIMARY KEY (`PRODUCTS_idProducts`,`PRODUCTS_TAGScol`),
  KEY `fk_table1_PRODUCTS1_idx` (`PRODUCTS_idProducts`),
  CONSTRAINT `fk_table1_PRODUCTS1` FOREIGN KEY (`PRODUCTS_idProducts`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stores` (
  `idStores` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `withdrawn` tinyint(1) NOT NULL DEFAULT '0',
  `typeOfStore` varchar(45) DEFAULT NULL,
  `registerDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `USERS_idUsers` int(11) NOT NULL,
  PRIMARY KEY (`idStores`,`USERS_idUsers`),
  UNIQUE KEY `idStores_UNIQUE` (`idStores`),
  KEY `fk_STORES_USERS_idx` (`USERS_idUsers`),
  CONSTRAINT `fk_STORES_USERS` FOREIGN KEY (`USERS_idUsers`) REFERENCES `users` (`idusers`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stores_has_products`
--

DROP TABLE IF EXISTS `stores_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stores_has_products` (
  `STORES_idStores` int(11) NOT NULL,
  `PRODUCTS_idProducts` int(11) NOT NULL,
  `price` varchar(45) DEFAULT NULL,
  `DateFrom` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateTo` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`STORES_idStores`,`PRODUCTS_idProducts`),
  KEY `fk_STORES_has_PRODUCTS_PRODUCTS1_idx` (`PRODUCTS_idProducts`),
  KEY `fk_STORES_has_PRODUCTS_STORES1_idx` (`STORES_idStores`),
  CONSTRAINT `fk_STORES_has_PRODUCTS_PRODUCTS1` FOREIGN KEY (`PRODUCTS_idProducts`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_STORES_has_PRODUCTS_STORES1` FOREIGN KEY (`STORES_idStores`) REFERENCES `stores` (`idstores`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stores_has_users_has_products`
--

DROP TABLE IF EXISTS `stores_has_users_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stores_has_users_has_products` (
  `STORES_idStores` int(11) NOT NULL,
  `STORES_USERS_idUsers` int(11) NOT NULL,
  `USERS_has_PRODUCTS_USERS_idUsers` int(11) NOT NULL,
  `USERS_has_PRODUCTS_PRODUCTS_idProducts` int(11) NOT NULL,
  PRIMARY KEY (`STORES_idStores`,`STORES_USERS_idUsers`,`USERS_has_PRODUCTS_USERS_idUsers`,`USERS_has_PRODUCTS_PRODUCTS_idProducts`),
  KEY `fk_STORES_has_USERS_has_PRODUCTS_STORES1_idx` (`STORES_idStores`,`STORES_USERS_idUsers`),
  CONSTRAINT `fk_STORES_has_USERS_has_PRODUCTS_STORES1` FOREIGN KEY (`STORES_idStores`, `STORES_USERS_idUsers`) REFERENCES `stores` (`idstores`, `users_idusers`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stores_tags`
--

DROP TABLE IF EXISTS `stores_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stores_tags` (
  `STORES_idStores` int(11) NOT NULL,
  `STORES_USERS_idUsers` int(11) NOT NULL,
  `tag` varchar(45) NOT NULL,
  PRIMARY KEY (`tag`,`STORES_idStores`),
  KEY `fk_table1_STORES1_idx` (`STORES_idStores`),
  CONSTRAINT `fk_table1_STORES1` FOREIGN KEY (`STORES_idStores`) REFERENCES `stores` (`idstores`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL AUTO_INCREMENT,
  `Blocked` tinyint(1) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `password` mediumtext NOT NULL,
  `userName` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `avatar` varchar(45) DEFAULT NULL,
  `registerDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idUsers`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_has_products`
--

DROP TABLE IF EXISTS `users_has_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users_has_products` (
  `USERS_idUsers` int(11) NOT NULL,
  `PRODUCTS_idProducts` int(11) NOT NULL,
  PRIMARY KEY (`USERS_idUsers`,`PRODUCTS_idProducts`),
  KEY `fk_USERS_has_PRODUCTS_PRODUCTS1_idx` (`PRODUCTS_idProducts`),
  KEY `fk_USERS_has_PRODUCTS_USERS1_idx` (`USERS_idUsers`),
  CONSTRAINT `fk_USERS_has_PRODUCTS_PRODUCTS1` FOREIGN KEY (`PRODUCTS_idProducts`) REFERENCES `products` (`id`),
  CONSTRAINT `fk_USERS_has_PRODUCTS_USERS1` FOREIGN KEY (`USERS_idUsers`) REFERENCES `users` (`idusers`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-12 18:00:23
