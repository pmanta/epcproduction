SELECT `p`.*, group_concat(`pt`.`PRODUCTS_TAGScol` separator ',') as `tags`
FROM PRODUCTS `p`
LEFT JOIN PRODUCTS_TAGS `pt`
ON p.idProducts = pt.PRODUCTS_idProducts
group by idProducts;