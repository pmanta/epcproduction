# EPCproduction


# Members

* Βαγγέλης Φέκας 03115043
* Παναγιώτης Μανταφούνης 03110682
* Κώστας Ταγαράκης 03115121
* Ρηγίνα-Μαρία Γεωργάνα 03113094
* Σπύρος Γιαννόπουλος 03115134
* Χριστόφορος Δομπρογιάννης 03111526

# Running the web application

`Clone the repository`

## Backend

```
cd epc-rest-api
mvn -Dmaven.test.failure.ignore=true clean package exec:java
```

## Redis-server

```
redis-server
```

## Frontend

```
cd epc-rest-ui
npm start
```

