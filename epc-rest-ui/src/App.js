import React, { Component } from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from './components/Home.jsx';
import ProductList from './components/ProductList.jsx';
import ShopList from './components/ShopList.jsx';
import Login from './components/Login.jsx';
import PrivateRoute from './components/PrivateRoute.jsx';
import PageDecorator from './components/PageDecorator.jsx';
import ProductNew from './components/ProductNew.jsx';
import ShopNew from './components/ShopNew.jsx';
import ProductDetails from './components/ProductDetails.jsx';
import ShopDetails from './components/ShopDetails.jsx';
import MapContainer from './components/MapContainer';
import AnonymousHeader from './components/AnonymousHeader.jsx';
import PriceNew from './components/PricesNew.jsx';


const NoMatch = () => (
  <div>
    <p>404</p>
  </div>
)

// const emptyDecorator = ({render: Render, ...rest}) => (
//   <div>
//     {Render()}
//   </div>
// )

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact render= {(props) => 
            <div>
              <AnonymousHeader />
              <Home />
            </div>
          } />
          <Route path="/login" exact component={Login} />
          
          <Route path="/map/:handle" exact render= {(props) => 
            <div>
              <AnonymousHeader />
              <MapContainer {...props} />
            </div>
          }  /> {/* here we just jender the map without the decorator */}
          <PrivateRoute path="/adminMap/:handle" exact decorator={PageDecorator} component={MapContainer} /> {/* Here we render the map in the admin page using the decorator */}
          <PrivateRoute path="/adminHome" exact decorator={PageDecorator} component={Home} />
          <PrivateRoute path="/products" exact decorator={PageDecorator} component={ProductList} />
          <PrivateRoute path="/products/new" exact decorator={PageDecorator} component={ProductNew} />
          <PrivateRoute path="/products/:handle" exact decorator={PageDecorator} component={ProductDetails} />
          <PrivateRoute path="/shops" exact decorator={PageDecorator} component={ShopList} />
          <PrivateRoute path="/shops/new" exact decorator={PageDecorator} component={ShopNew} />
          <PrivateRoute path="/shops/:handle" exact decorator={PageDecorator} component={ShopDetails} />
          <PrivateRoute path="/prices/new" exact decorator={PageDecorator} component={PriceNew} />
          <PrivateRoute decorator={PageDecorator} component={NoMatch} />
        </Switch>
      </Router>
    );
  }
}

export default App;
