import React, { Component } from "react";
import * as functions from "./FunctionLibrary";
import { ToastContainer } from 'react-toastr';

class ProductNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            withdrawn: "false",
            origin: "true",
            description: "",
            tags: []
        };
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    addProductTag() {
        // the tag that needs to be added is in this.state.tag2add
        if (!!!this.state.tag2add) {
            window.alert("No tag to add to product!");
            return;
        }
        let { tags } = this.state;
        tags.push(this.state.tag2add);

        this.update_state({
            tags: tags,
            tag2add: ""
        });
    }

    removeProductTag(index) {
        console.log("Remove product tag number " + index);
        var { tags } = this.state;
        tags.splice(index, 1);

        this.update_state({
            tags: tags
        });
    }

    registerProduct(e,toastContainer) {

        if (!!!this.state.name || !!!this.state.category) {
            toastContainer.error("Fill all required fields.", "Error!", {
                closeButton: true
            })
            return;
        }

        var obj = {
            name: this.state.name,
            description: this.state.description,
            category: this.state.category,
            origin: (this.state.origin === 'true'),
            withdrawn: (this.state.withdrawn === 'true'),
            tags: this.state.tags
        }

        console.log(obj);
        functions.http_post_urlencoded('/products', obj)
        .then(json => {
            console.log("got ", json)
            toastContainer.success("Registered product.", "Success!", {
                closeButton: true
            })

            // Empty all fields in the form for adding a new product
            this.update_state({
                name: "",
                description: "",
                category: "",
                origin: "true",
                withdrawn: "false",
                tags: []
            })
        })
        .catch(status => {
            console.log("what is this?")
            toastContainer.error("Status " + status, "Error!", {
                closeButton: true
            })
        })
    }

    render() {
        let container;
        return (
            <div>
                <ToastContainer
                    ref={ref => container = ref}
                    className="toast-top-right"
                />
                
                <section className="content-header">
                    <h1>
                        Products
                        <small>add new product</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">Name*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="name"
                                    placeholder="Enter Name"
                                    value={this.validateContext(this.state.name)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="category">Category*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="category"
                                    placeholder="Enter Category"
                                    value={this.validateContext(this.state.category)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="description">Description</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="description"
                                    placeholder="Enter Description"
                                    value={this.validateContext(this.state.description)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-4">
                                    <label>Withdrawn</label>
                                    <select 
                                        className="form-control" 
                                        id="withdrawn"
                                        onChange={this.propertyChanged} 
                                        value={this.validateContext(this.state.withdrawn)}
                                    >
                                        <option>true</option>
                                        <option>false</option> 
                                    </select>
                                    
                                    <p className="help-block">
                                        Check if the product is withdrawn.
                                    </p>
                                </div>

                                <div className="form-group col-xs-4">
                                    <label>Domestic</label>
                                    <select 
                                        className="form-control" 
                                        id="origin"
                                        onChange={this.propertyChanged} 
                                        value={this.validateContext(this.state.origin)}
                                    >
                                        <option>true</option>
                                        <option>false</option> 
                                    </select>
                                    
                                    <p className="help-block">
                                        Check if the product is domestic.
                                    </p>
                                </div>
                            </div>
                            <div className="input-group input-group-sm col-xs-3">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="tag2add"
                                    value={this.validateContext(this.state.tag2add)}
                                    onChange={this.propertyChanged}
                                    placeholder="Add tag to product"
                                />
                                <label />
                                <span className="input-group-btn">
                                    <button
                                        type="button"
                                        className="btn bg-olive btn-flat"
                                        onClick={this.addProductTag.bind(this)}
                                    >
                                        <i className="fa fa-fw fa-plus"></i>
                                    </button>
                                </span>
                            </div>
                            {this.state.tags
                                .map((item, index) => {
                                    return (
                                        <label key={index}>
                                            <div className="label bg-olive">
                                                {item} <i className="fa fa-fw fa-trash" onClick={() => this.removeProductTag.bind(this)(index)}></i>
                                            </div>
                                            &nbsp;
                                        </label>
                                    )
                                })
                            }
                        </div>
                        <div className="box-footer">
                            <button 
                                type="submit" 
                                className="btn btn-primary"
                                onClick={(e) => this.registerProduct.bind(this)(e, container)}
                            >
                                Submit
                            </button>
                        </div>
                    </div>
                    
                </section>
            </div>
        );
    }
}

export default ProductNew;
