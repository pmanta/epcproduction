import React, { Component } from 'react';
import * as functions from './FunctionLibrary.jsx';
import Loading from './Loading.jsx';
import CustomHtmlTableS from './CustomHtmlTableS.jsx';

class ShopList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            shops: [],
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
        
        // Get the shop List from the api
        functions.http_get_json("/shops?status=ALL")
        .then(json => {
            this.update_state({
                isLoaded: true,
                shops: json.shops,
            })
        })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const {isLoaded, shops} = this.state;

        if (!isLoaded)
            return (<Loading></Loading>)
        else 
            return (
                <div>
                    <section className="content-header">
                        <h1>Shops
                            <small>preview of shops</small>
                        </h1>
                        
                    </section>
                    <section className="content">
                        <CustomHtmlTableS shops={shops}/>
                    </section>
                </div>
            );
    }
}

export default ShopList;