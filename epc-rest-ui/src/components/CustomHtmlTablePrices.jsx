import React, { Component } from 'react'
import '../css/custom.css'
import {Link} from 'react-router-dom'

class CustomHtmlTablePrices extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    processWithdrawn(item) {
        return item.withdrawn ? "fa fa-circle text-danger" : "fa fa-circle text-success"
    }

    render() {
        const { prices } = this.props
        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th>Product ID</th>
                                <th>Product Name</th>
                                <th>Product Tags</th>
                                <th>Shop Id</th>
                                <th>Shop Name</th>
                                <th>Shop Address</th>
                                <th>Shop Tags</th>
                                <th>Price</th>
                                <th>Date</th>
                            </tr>

                            {prices.map((item, index) => (
                                <tr key={index}>
                                    <td>{item.productId}</td>
                                    <td>{item.productName}</td>
                                    <td>{!!item.productTags ? item.productTags.join(",") : ""}</td>
                                    <td>{item.shopId}</td>
                                    <td>{item.shopName}</td>
                                    <td>
                                        <Link to={!!!this.props.location ? `/map/${item.shopId}` : `/adminMap/${item.shopId}`}>
                                            {item.shopAddress}&nbsp;&nbsp;
                                            <i className="fa fa-map-marker"></i>
                                        </Link>
                                    </td>
                                    <td>{!!item.shopTags ? item.shopTags.join(',') : ""}</td>
                                    <td>{item.price}</td>
                                    <td>{item.date}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTablePrices
