import React, { Component } from 'react';
import * as functions from './FunctionLibrary.jsx';
import Loading from './Loading.jsx';
import CustomHtmlTable from './CustomHtmlTable.jsx';

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            products: [],
        }

        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
        
        // Get the product List from the api
        functions.http_get_json("/products?status=ALL")
        .then(json => {
            this.update_state({
                isLoaded: true,
                products: json.products,
            })
        })
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    render() {
        const {isLoaded, products} = this.state;

        if (!isLoaded)
            return (<Loading></Loading>)
        else 
            return (
                <div>
                    <section className="content-header">
                        <h1>Products
                            <small>preview of products</small>
                        </h1>
                        
                    </section>
                    <section className="content">
                        <CustomHtmlTable products={products}/>
                    </section>
                </div>
            );
    }
}
 
export default ProductList;