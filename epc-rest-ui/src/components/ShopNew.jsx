import React, { Component } from "react";
import * as functions from "./FunctionLibrary";
import { ToastContainer } from "react-toastr";

class ShopNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            withdrawn: "false",
            tags: []
        };
        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true;
    }

    componentWillUnmount() {
        this.mounted = false;
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state);
    }

    addShopTag() {
        // the tag that needs to be added is in this.state.tag2add
        if (!!!this.state.tag2add) {
            window.alert("No tag to add to the shop!");
            return;
        }
        let { tags } = this.state;
        tags.push(this.state.tag2add);

        this.update_state({
            tags: tags,
            tag2add: ""
        });
    }

    removeShopTag(index) {
        console.log("Remove shop tag number " + index);
        var { tags } = this.state;
        tags.splice(index, 1);

        this.update_state({
            tags: tags
        });
    }

    registerShop(e, toastContainer) {
        if (
            !!!this.state.name ||
            !!!this.state.address ||
            !!!this.state.lng ||
            !!!this.state.lat
        ) {
            toastContainer.error("Fill all required fields.", "Error!", {
                closeButton: true
            });
            return;
        }

        var obj = {
            name: this.state.name,
            address: this.state.address,
            lng: this.state.lng,
            lat: this.state.lat,
            withdrawn: this.state.withdrawn === "true",
            tags: this.state.tags
        };

        console.log(obj);
        functions
            .http_post_urlencoded("/shops", obj)
            .then(json => {
                console.log("got ", json);
                toastContainer.success("Registered shop.", "Success!", {
                    closeButton: true
                });

                // Empty all fields in the form for adding a new shop
                this.update_state({
                    name: "",
                    withdrawn: "false",
                    address: "",
                    lng: null,
                    lat: null,
                    tags: []
                });
            })
            .catch(status => {
                console.log("what is this?");
                toastContainer.error("Status " + status, "Error!", {
                    closeButton: true
                });
            });
    }

    render() {
        let container;
        return (
            <div>
                <ToastContainer
                    ref={ref => (container = ref)}
                    className="toast-top-right"
                />

                <section className="content-header">
                    <h1>
                        Shops
                        <small>add new shop</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="name">Name*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="name"
                                    placeholder="Enter Name"
                                    value={this.validateContext(this.state.name)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="address">Address*</label>
                                <input
                                    type="text"
                                    required
                                    className="form-control"
                                    id="address"
                                    placeholder="Enter Address"
                                    value={this.validateContext(this.state.address)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-4">
                                    <label>Latitude*</label>
                                    <input
                                        type="number"
                                        required
                                        className="form-control"
                                        id="lat"
                                        placeholder="Enter Latitude"
                                        value={this.validateContext(this.state.lat)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                                <div className="form-group col-xs-4">
                                    <label>Longtitude*</label>
                                    <input
                                        type="number"
                                        required
                                        className="form-control"
                                        id="lng"
                                        placeholder="Enter Longtitude"
                                        value={this.validateContext(this.state.lng)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Withdrawn</label>
                                <select
                                    className="form-control"
                                    id="withdrawn"
                                    onChange={this.propertyChanged}
                                    value={this.validateContext(this.state.withdrawn)}
                                >
                                    <option>true</option>
                                    <option>false</option>
                                </select>

                                <p className="help-block">
                                    Check if the shop is withdrawn.
                                </p>
                            </div>
                            <div className="input-group input-group-sm col-xs-3">
                                <input
                                    type="text"
                                    className="form-control"
                                    id="tag2add"
                                    value={this.validateContext(this.state.tag2add)}
                                    onChange={this.propertyChanged}
                                    placeholder="Add tag to shop"
                                />
                                <label />
                                <span className="input-group-btn">
                                    <button
                                        type="button"
                                        className="btn bg-olive btn-flat"
                                        onClick={this.addShopTag.bind(this)}
                                    >
                                        <i className="fa fa-fw fa-plus" />
                                    </button>
                                </span>
                            </div>
                            {this.state.tags.map((item, index) => {
                                return (
                                    <label key={index}>
                                        <div className="label bg-olive">
                                            {item}{" "}
                                            <i
                                                className="fa fa-fw fa-trash"
                                                onClick={() =>
                                                    this.removeShopTag.bind(this)(index)
                                                }
                                            />
                                        </div>
                                        &nbsp;
                                    </label>
                                );
                            })}
                        </div>
                        <div className="box-footer">
                            <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={e =>
                                    this.registerShop.bind(this)(e, container)
                                }
                            >
                                Submit
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default ShopNew;
