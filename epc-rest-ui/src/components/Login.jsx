import React, { Component } from 'react'
import LoginForm from './LoginForm'
import { Link } from 'react-router-dom'
class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <div className="hold-transition login-page">
                <div className="login-box">
                    <div className="login-logo">
                        <Link to="/"><b>Carnivore</b>EPC</Link>
                    </div>
                    <LoginForm {...this.props}></LoginForm>
                </div>
            </div>
        )
    }
}

export default Login
