import React, { Component } from 'react'
import * as functions from './FunctionLibrary'
import GoogleMapReact from 'google-map-react'


/*@handle which is the shop to center or all if all should be shown*/
class MapContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoaded: false,
        }

        this.update_state = functions.update_state.bind(this)
        this.renderShops = this.renderShops.bind(this)
    }

    static defaultProps = {
        center: {
            lat: 38.3515998,
            lng: 23.8418913,
        },
        zoom: 8,
    }

    // pass new features from props into the OpenLayers layer object
    componentDidUpdate(prevProps, prevState) {
    }

    componentDidMount() {
        this.mounted = true
    }

    componentWillUnmount() {
        this.mounted = false
    }

    renderMarkers(map, maps) {
        console.log('render shops')
    }

    renderShops(map, maps, obj) {
        var infowindow
        /* For every shop create a marker */
        functions.http_get_json("/shops")
        .then(json => {
            json.shops.forEach((value, index) => {
                console.log("Creating marker for ", value)
                let marker = new maps.Marker({
                    position: {
                        lat: parseFloat(value.lat),
                        lng: parseFloat(value.lng),
                    },
                    map: map,
                    // icon: {
                    //     scaledSize: new maps.Size(30, 30),
                    // },
                    title: value.name,
                })

                marker.addListener('click', function() {
                    if (infowindow) {
                        infowindow.close()
                    }

                    infowindow = new maps.InfoWindow({
                        content: functions.createInfoWindowString(value),
                    })

                    infowindow.open(map, marker)
                })

                if (value.id === this.props.match.params.handle) {
                    this.update_state({
                        center: {
                            lat: value.lat,
                            lng: value.lng,
                        },
                    })
                    maps.event.trigger(marker, 'click')
                }
            })
        })
    }

    static defaultProps = {
        center: {
            lat: 38.3515998,
            lng: 23.8418913,
        },
        zoom: 8,
    }

    render() {
        const options = {
            maxZoom: 50,
        }
        return (
            // Important! Always set the container height explicitly
            <div style={{ height: '91vh', width: '100%' }}>
                <GoogleMapReact
                    bootstrapURLKeys={{
                        key: 'AIzaSyCuorFkqQeyvEDeYq6V7sg9zYKA1FUc028'
                    }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    center={this.state.center}
                    zoom={this.state.zoom}
                    defaultOptions={options}
                    onGoogleApiLoaded={({ map, maps }) =>
                        this.renderShops.bind(this)(map, maps)
                    }
                    yesIWantToUseGoogleMapApiInternals // supress the warning for API internals
                />
            </div>
        )
    }
}

export default MapContainer
