import React, { Component } from 'react'
import { Redirect, Route } from 'react-router-dom'
import { validateToken, update_state } from './FunctionLibrary.jsx'

class PrivateRoute extends Component {
    state = {}

    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            isAuthenticated: false,
        }
        this.updateState = update_state.bind(this)
    }

    componentDidMount() {
        this.mounted = true
        validateToken().then(status => {
            var auth = false
            if (status === 200) {
                auth = true
            }

            this.updateState({
                isLoading: false,
                isAuthenticated: auth,
            })
        })
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.props, prevProps)
        if (prevProps.path !== this.props.path) {
            validateToken().then(status => {
                var auth = false
                if (status === 200) {
                    auth = true
                } else {
                    localStorage.removeItem('accessToken')
                }
                this.updateState({
                    isAuthenticated: auth,
                })
            })
        }
    }

    componentWillUnmount() {
        this.mounted = false
    }
    /* <Decorator render={(props) => (<Loading></Loading>)} /> */

    render() {
        console.log("Rendering Private route")
        const {
            component: Component,
            render: RenderMethod,
            decorator: Decorator,
            ...rest
        } = this.props

        // Check if there is a token if not redirect to login
        console.log(localStorage.getItem('accessToken'))
        if (localStorage.getItem('accessToken') == null) {
            console.log("there is no localStorage!!!")
            return (
                <Redirect
                    to={{
                        pathname: '/login',
                        state: { from: this.props.location },
                    }}
                />
            )
            // window.href = "/login";
        }
        if (this.state.isLoading) return null
        else
            return (
                <Decorator
                    render={props => (
                        <Route
                            {...rest}
                            render={props => {
                                if (this.state.isAuthenticated) {
                                    if (Component !== undefined)
                                        return <Component {...props} />
                                    else return RenderMethod()
                                } else
                                    return (
                                        <Redirect
                                            to={{
                                                pathname: '/login',
                                                state: { from: props.location },
                                            }}
                                        />
                                    )
                            }}
                        />
                    )}
                />
            )
    }
}

export default PrivateRoute
