import React, { Component } from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import '../css/datepicker-overrides.css'
import * as functions from './FunctionLibrary'
import CustomHtmlTablePrices from './CustomHtmlTablePrices';

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            geoLat: '0.001',
            geoDist: '0.001',
            geoLng: '0.001',
            products: [],
            shops: [],
            tags: [],
            showResults: false,
            prices: []
        }

        this.update_state = functions.update_state.bind(this)
        this.propertyChanged = functions.propertyChanged.bind(this)
        this.arrayChanged = this.arrayChanged.bind(this)
    }

    componentDidMount() {
        this.mounted = true
    }

    componentWillUnmount() {
        this.mounted = false
    }

    componentDidUpdate() {
        console.log(this.state);
    }

    dateChanged(key, value) {
        var tmp = {}
        tmp[key] = value
        this.update_state(tmp)
    }

    arrayChanged(event) {
        var id = event.target.id
        var value = event.target.value
        var tmp = {}
        tmp[id+"String"] = value
        /* Pro */
        tmp[id] = value.split(/\s*,\s*/);
        this.update_state(tmp)
    }

    digitify(value) {
        return ("0" + value).slice(-2)
    }

    extractNeededDate(date) {
        var year = date.getFullYear()
        var month = this.digitify(date.getMonth() + 1)
        var day = this.digitify(date.getDate())

        return year + '-' + month + '-' + day
    }

    searchPrices() {
        // Parse the dates to the YYYY-MM-dd format with regex
        var obj = {
            dateFrom: this.extractNeededDate(this.state.dateFrom),
            dateTo: this.extractNeededDate(this.state.dateTo),
            geoLat: parseFloat(this.state.geoLat),
            geoLng: parseFloat(this.state.geoLng),
            geoDist: parseFloat(this.state.geoDist),
            products: this.state.products,
            shops: this.state.shops,
            tags: this.state.tags
        }

        console.log(obj)
        console.log(functions.urlencode(obj))

        functions.http_get_json("/prices?"+functions.urlencode(obj))
        .then(json => {
            console.log("Got : ", json)
            this.update_state({
                prices: json.prices,
                showResults: true
            })
        })
    }

    render() {
        const { prices } = this.state
        return (
            <section className="content">
                <div className="box box-primary">
                    <div className="box-header">
                        <h3 className="box-title">Search</h3>
                    </div>
                    <div className="box-body">
                        <div className="form-group col-xs-2">
                            <label>Date From:</label>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    <i className="fa fa-calendar" />
                                </div>
                                <DatePicker
                                    onChange={value =>
                                        this.dateChanged.bind(this)(
                                            'dateFrom',
                                            value
                                        )
                                    }
                                    selected={this.state.dateFrom}
                                    dateFormat="YYYY-MM-dd"
                                    className="custom-datepicker"
                                />
                            </div>
                        </div>
                        <div className="form-group col-xs-2">
                            <label>Date To:</label>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    <i className="fa fa-calendar" />
                                </div>
                                <DatePicker
                                    onChange={value =>
                                        this.dateChanged.bind(this)(
                                            'dateTo',
                                            value
                                        )
                                    }
                                    selected={this.state.dateTo}
                                    dateFormat="YYYY-MM-dd"
                                    className="custom-datepicker"
                                />
                            </div>
                        </div>
                        <div className="form-group col-xs-2">
                            <label>Geo Lat:</label>

                            <div className="input-group">
                                <div className="input-group-addon">
                                    <i className="fa fa-map-marker" />
                                </div>
                                <input
                                    type="number"
                                    className="form-control pull-right"
                                    id="geoLat"
                                    value={functions.validateContext(this.state.geoLat)}
                                    onChange={this.propertyChanged}
                                    step="0.00001"
                                />
                            </div>
                        </div>
                        <div className="form-group col-xs-2">
                            <label>Geo Lng:</label>

                            <div className="input-group">
                                <div className="input-group-addon">
                                    <i className="fa fa-map-marker" />
                                </div>
                                <input
                                    type="number"
                                    className="form-control pull-right"
                                    id="geoLng"
                                    value={functions.validateContext(this.state.geoLng)}
                                    onChange={this.propertyChanged}
                                    step="0.00001"
                                />
                            </div>
                        </div>
                        <div className="form-group col-xs-2">
                            <label>Dist(Km):</label>

                            <div className="input-group">
                                <div className="input-group-addon">
                                    <i className="fa fa-arrows-h" />
                                </div>
                                <input
                                    type="number"
                                    className="form-control pull-right"
                                    id="geoDist"
                                    step="0.001"
                                    value={functions.validateContext(this.state.geoDist)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                        </div>
                        <div className="form-group col-xs-1">
                            <label>PID(s):</label>

                            <div className="input-group">
                                <input
                                    type="text"
                                    className="form-control pull-right"
                                    id="products"
                                    value={functions.validateContext(this.state.productsString)}
                                    onChange={(e) => this.arrayChanged(e)}
                                />
                            </div>
                        </div>
                        <div className="form-group col-xs-1">
                            <label>Shop ID(s):</label>

                            <div className="input-group">
                                <input
                                    type="text"
                                    className="form-control pull-right"
                                    id="shops"
                                    value={functions.validateContext(this.state.shopsString)}
                                    onChange={(e) => this.arrayChanged(e)}
                                />
                            </div>
                        </div>
                        <div className="form-group col-xs-2">
                            <label>Tags:</label>

                            <div className="input-group">
                                <input
                                    type="text"
                                    className="form-control pull-right"
                                    id="tags"
                                    value={functions.validateContext(this.state.tagsString)}
                                    onChange={(e) => this.arrayChanged(e)}
                                />
                            </div>
                        </div>
                        <div
                            className="form-group col-xs-1"
                            style={{ paddingTop: '24px' }}
                        >
                            {/* <label>Magic</label> */}
                            <div className="input-group">
                                <button
                                    className="btn btn-block btn-primary"
                                    type="button"
                                    style={{ width: '100px' }}
                                    onClick={this.searchPrices.bind(this)}
                                >
                                    Magic <i className="fa fa-search" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                
                {this.state.showResults && <CustomHtmlTablePrices prices={prices} {...this.props}></CustomHtmlTablePrices>}
                
            </section>
        )
    }
}

export default Home
