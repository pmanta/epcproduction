import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class AnonymousHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <header className="main-header">
            <Link to="/" className="logo">
                <span className="logo-mini"><b>C</b>EPC</span>
                <span className="logo-lg"><b>Carnivore</b>EPC</span>
            </Link>

            <nav className="navbar navbar-static-top">
            <div className="navbar-custom-menu">
                <ul className="nav navbar-nav">
                <li className="dropdown user user-menu">
                    <Link to="/map/all">
                        Map
                    </Link>
                </li>
                <li className="dropdown user user-menu">
                    <Link to="/login">
                        login
                    </Link>
                </li>
                </ul>
            </div>
        </nav>
        </header>
        );
    }
}
 
export default AnonymousHeader;