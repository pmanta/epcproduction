import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import * as functions from './FunctionLibrary'

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            username: 'username'
        }

        this.update_state = functions.update_state.bind(this)
    }

    componentDidMount() {
        this.mounted = true;
        // Extract the username from the jwt token
        this.update_state({
            username: localStorage.getItem("username")
        })
    }

    render() { 
        return (
            <section className="sidebar">
            <div className="user-panel">
                <div className="pull-left image">
                    <img src="/dist/img/user2-160x160.jpg" className="img-circle" alt="User"/>
                </div>
                <div className="pull-left info">
                <p>{this.state.username}</p>
                <a href="# "><i className="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <form action="# " method="get" className="sidebar-form">
                <div className="input-group">
                <input type="text" name="q" className="form-control" placeholder="Search..."/>
                <span className="input-group-btn">
                        <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
            <ul className="sidebar-menu" data-widget="tree">
                <li className="header">MAIN NAVIGATION</li>
                <li><Link to="/adminHome"><i className="fa fa-dashboard"></i> Home</Link></li>
                <li className="treeview">
                <a href="# ">
                    <i className="fa fa-archive"></i> <span>Products</span>
                    <span className="pull-right-container">
                    <i className="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul className="treeview-menu">
                    <li><Link to="/products"><i className="fa fa-fw fa-table"></i>Products List</Link></li>
                    <li><Link to="/products/new"><i className="fa fa-fw fa-plus-square"></i>Add new</Link></li>
                </ul>
                </li>
                <li className="treeview">
                <a href="# ">
                    <i className="fa fa-building"></i> <span>Shops</span>
                    <span className="pull-right-container">
                    <i className="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul className="treeview-menu">
                    <li><Link to="/shops"><i className="fa fa-fw fa-table"></i>Shops List</Link></li>
                    <li><Link to="/shops/new"><i className="fa fa-fw fa-plus-square"></i>Add new</Link></li>
                </ul>
                </li>
                <li className="treeview">
                    <a href="# ">
                        <i className="fa fa-user-o"></i> <span>Users</span>
                        <span className="pull-right-container">
                        <i className="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul className="treeview-menu">
                        <li><a href="# "><i className="fa fa-fw fa-table"></i> List Users </a></li>
                        <li><a href="# "><i className="fa fa-fw fa-plus-square"></i> Add new </a></li>
                    </ul>
                </li>
                <li className="treeview">
                    <a href="# ">
                        <i className="fa fa-euro"></i> <span>Prices</span>
                        <span className="pull-right-container">
                        <i className="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul className="treeview-menu">
                        <li><Link to="/prices/new"><i className="fa fa-fw fa-plus-square"></i> Add new </Link></li>
                    </ul>
                </li>
                <li>
                    <Link to="/adminMap/all">
                        <i className="fa fa-map-o"></i> <span> Map </span>
                        <span className="pull-right-container">
                        </span>
                    </Link>
                    
                </li>
            </ul>
            </section>
        );
    }
}
 
export default Sidebar;