import React, { Component } from 'react'
import '../css/custom.css'
import {Link} from 'react-router-dom'

class CustomHtmlTable extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    processWithdrawn(item) {
        return item.withdrawn ? "fa fa-circle text-danger" : "fa fa-circle text-success"
    }

    render() {
        const { products } = this.props
        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th>Product ID</th>
                                <th>Product Name</th>
                                <th>Product Description</th>
                                <th>Product Category</th>
                                <th>Product Origin</th>
                                <th>Product Photo</th>
                                <th>Product Tags</th>
                                <th>Status</th>
                            </tr>

                            {products.map((item, index) => (
                                <tr key={item.id}>
                                    <td><Link to={`/products/${item.id}`}>{item.id}</Link></td>
                                    <td>{item.name}</td>
                                    <td>{item.description}</td>
                                    <td>{item.category}</td>
                                    <td>{item.origin ? "Domestic" : "Imported"}</td>
                                    <td>{item.photo}</td>
                                    <td>{item.tags.join(',')}</td>
                                    <td>
                                        <i className={this.processWithdrawn(item)}></i>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTable
