import React, { Component } from 'react'
import Header from './Header'
import Sidebar from './Sidebar'
import { loadScript } from './FunctionLibrary'
import { update_state } from './FunctionLibrary'

class PageDecorator extends Component {
    state = {}

    constructor(props) {
        super(props)
        this.state = {
            scriptLoaded: false,
        }
        this.update_state = update_state.bind(this)
        this.callback = this.callback.bind(this)
    }

    callback () {
        // trigger an update in the state
        this.update_state({
            scriptLoaded: true
        })
    }

    componentDidMount() {
        this.mounted = true
        // Load the script in order to decorate the css classes using jquery
        loadScript('/dist/js/adminlte.min.js', this.callback)
    }

    componentWillUnmount() {
        this.mounted = false
        console.log("PageDecorator will unmount");
    }

    render() {
        const { render } = this.props
        return (
            <div className="wrapper">
                    <Header/>
                <aside className="main-sidebar">
                    <Sidebar/>
                </aside>
                <div className="content-wrapper">
                    {render()}
                </div>
            </div>
        )
    }
}

export default PageDecorator;