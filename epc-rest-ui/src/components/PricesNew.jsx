import React, { Component } from 'react';
import * as functions from "./FunctionLibrary";
import { ToastContainer } from 'react-toastr';
import DatePicker from 'react-datepicker';

class PriceNew extends Component {
    constructor(props) {
        super(props);
        this.state = {  }

        this.propertyChanged = functions.propertyChanged.bind(this);
        this.validateContext = functions.validateContext.bind(this);
        this.update_state = functions.update_state.bind(this);
    }

    componentDidMount() {
        this.mounted = true
    }

    componentWillUnmount() {
        this.mounted = false
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.state)    
    }

    dateChanged(key, value) {
        var tmp = {}
        tmp[key] = value
        this.update_state(tmp)
    }

    digitify(value) {
        return ("0" + value).slice(-2)
    }

    extractNeededDate(date) {
        var year = date.getFullYear()
        var month = this.digitify(date.getMonth() + 1)
        var day = this.digitify(date.getDate())

        return year + '-' + month + '-' + day
    }

    registerPrice(e,toastContainer) {

        if (
            !!!this.state.dateFrom ||
            !!!this.state.dateTo ||
            !!!this.state.productId ||
            !!!this.state.shopId
        ) {
            toastContainer.error("Fill all required fields.", "Error!", {
                closeButton: true
            })
            return;
        }

        var obj = {
            dateFrom: this.extractNeededDate(this.state.dateFrom),
            dateTo: this.extractNeededDate(this.state.dateTo),
            productId: this.state.productId,
            shopId: this.state.shopId,
            price: parseFloat(this.state.price)
        }

        console.log(obj);
        functions.http_post_urlencoded('/prices', obj)
        .then(json => {
            console.log("got ", json)
            toastContainer.success("Registered price.", "Success!", {
                closeButton: true
            })

            // Empty all fields in the form for adding a new product
            this.update_state({
                price: "",
                productId: "",
                shopId: ""
            })
        })
        .catch(status => {
            console.log("what is this?")
            toastContainer.error("Status " + status, "Error!", {
                closeButton: true
            })
        })
    }

    render() { 
        let container;
        return (
            <div>
                <ToastContainer
                    ref={ref => container = ref}
                    className="toast-top-right"
                />
                
                <section className="content-header">
                    <h1>
                        Prices
                        <small>add new price</small>
                    </h1>
                </section>
                <section className="content">
                    <div className="box box-primary">
                        {/* <div className="box-header with-border">
                            <h3 className="box-title">Add</h3>
                        </div> */}
                        <div className="box-body">
                            <div className="form-group">
                                <label htmlFor="dateFrom">Date From *</label>
                                <div>
                                <DatePicker 
                                    onChange={value =>
                                        this.dateChanged.bind(this)(
                                            'dateFrom',
                                            value
                                        )
                                    }
                                    selected={this.state.dateFrom}
                                    dateFormat="YYYY-MM-dd"
                                />
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="dateFrom">Date To *</label>
                                <div>
                                <DatePicker 
                                    onChange={value =>
                                        this.dateChanged.bind(this)(
                                            'dateTo',
                                            value
                                        )
                                    }
                                    selected={this.state.dateTo}
                                    dateFormat="YYYY-MM-dd"
                                />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-xs-4">
                                    <label htmlFor="productId">Product Id *</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        id="productId"
                                        placeholder="Enter Product Id"
                                        value={this.validateContext(this.state.productId)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                                <div className="form-group col-xs-4">
                                    <label htmlFor="shopId">Shop Id *</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        id="shopId"
                                        placeholder="Enter Shop Id"
                                        value={this.validateContext(this.state.shopId)}
                                        onChange={this.propertyChanged}
                                    />
                                </div>
                            </div>
                            <div className="row">
                            <div className="form-group col-xs-4">
                                <label htmlFor="price">Price</label>
                                <input
                                    type="number"
                                    className="form-control"
                                    id="price"
                                    placeholder="Enter Price"
                                    step="0.01"
                                    value={this.validateContext(this.state.price)}
                                    onChange={this.propertyChanged}
                                />
                            </div>
                            </div>
                        </div>
                        <div className="box-footer">
                            <button 
                                type="submit" 
                                className="btn btn-primary"
                                onClick={(e) => this.registerPrice.bind(this)(e, container)}
                            >
                                Submit
                            </button>
                        </div>
                    </div>
                    
                </section>
            </div>
        );
    }
}
 
export default PriceNew;