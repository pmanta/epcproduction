import React, { Component } from 'react'
import * as functions from './FunctionLibrary'

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            userAuthenticated: false,
            authenticationError: false,
        }
        this.update_state = functions.update_state.bind(this)
        this.propertyChanged = functions.propertyChanged.bind(this)
    }

    componentDidMount() {
        this.mounted = true

        // load js script for the checkbox
        // functions.loadScript("/plugins/iCheck/icheck.min.js", function () {});
        functions.loadScript("/js/login_check.js", function () {});
    }

    componentWillUnmount() {
        this.mounted = false
    }

    componentDidUpdate() {
        console.log(this.state)
    }

    loginUser(e) {
        const { from } = this.props.location.state || { from: { pathname: '/adminHome' } }

        functions.authenticate(
            this.state.username,
            this.state.password
        )
            .then(res => {
                console.log(res)
                if (!!res.error) {
                    console.log("invalid credentials")
                    this.update_state({ authenticationError: true})
                }
                else {
                    console.log("success credentials")
                    localStorage.setItem('accessToken', res.token)
                    localStorage.setItem('username', this.state.username)
                    window.location.href = from.pathname
                }
            })

        e.preventDefault()
    }

    render() {
        return (
            <div className="login-box-body">
                {this.state.authenticationError && (
                        
                        <div className="alert alert-danger">
                            <i className="icon fa fa-ban"/>
                            <span> Invalid credentials </span>
                        </div>
                )}
            <form>
                <p className="login-box-msg">Sign in to start your session</p>
                <div className="form-group has-feedback">
                    <input
                        id="username"
                        type="text" 
                        className="form-control" 
                        placeholder="Username"
                        value={this.state.username}
                        onChange={this.propertyChanged}
                    />
                    <span className="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div className="form-group has-feedback">
                    <input 
                        id="password"
                        type="password" 
                        className="form-control" 
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.propertyChanged}
                    />
                    <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div className="row">
                    <div className="col-xs-8">
                        <div className="checkbox icheck">
                            <label>
                                <input type="checkbox"/>Remember Me
                            </label>
                        </div>
                    </div>
                    <div className="col-xs-4">
                        <button 
                            type="submit" 
                            className="btn btn-primary btn-block btn-flat"
                            onClick={(e) => this.loginUser(e)}
                        >
                            Sign In
                        </button>
                    </div>
                </div>
            </form>
                <div className="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="# " className="btn btn-block btn-social btn-facebook btn-flat"><i className="fa fa-facebook"></i> Sign in using
                        Facebook</a>
                    <a href="# " className="btn btn-block btn-social btn-google btn-flat"><i className="fa fa-google-plus"></i> Sign in using
                        Google+</a>
                </div>

                <a href="# ">I forgot my password</a><br/>
                <a href="register.html" className="text-center">Register a new membership</a>

            </div>
        )
    }
}

export default LoginForm
