
const apiPath = "https://localhost:8765/observatory/api"

function handleErrors(res) {
    if (!res.ok)
        throw res.status
    
    return res
}

/* Function that updates the state if this.mounted is set
 * needs to be bound to the component that it gets called
*/
export function update_state(state)
{
    if (this.mounted)
        this.setState(state);
}

/* http_get request for the ui with the headers that we
 * need to set and the error handling
 */
export async function http_get_json(endpoint)
{
    // Force no trailing slash at the start of the endpoint
    if (endpoint.startsWith("/"))
        endpoint = endpoint.substring(1);

    console.log("hello");
    return fetch(apiPath + "/" + endpoint, {
        // 'mode': 'cors', //maybe not needed
        'headers': {
            // 'authorisation': localStorage.getItem("tokenType") + " " + localStorage.getItem("accessToken"),
        }
    })
    .then(res => res.json())
}

export async function http_post_urlencoded(endpoint, data) {
    // Force no trailing slash at the start of the endpoint
    if (endpoint.startsWith("/"))
        endpoint = endpoint.substring(1);

    console.log("http_post")
    return fetch(apiPath + "/" + endpoint, {
        'method': 'POST',
        'headers': {
            'X-OBSERVATORY-AUTH': localStorage.getItem("accessToken"),
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        'body': urlencode(data) //The data should be JSON.stringified before comming here
    })
        .then(handleErrors)
        .then(res => Promise.resolve(res.json()))
        .catch(status => {
            console.log("Something happened : ", status)
            throw status;
        })
}

export async function http_put_urlencoded(endpoint, data) {
    // Force no trailing slash at the start of the endpoint
    if (endpoint.startsWith("/"))
        endpoint = endpoint.substring(1);

    console.log("http_put")
    return fetch(apiPath + "/" + endpoint, {
        'method': 'PUT',
        'headers': {
            'X-OBSERVATORY-AUTH': localStorage.getItem("accessToken"),
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        'body': urlencode(data) //The data should be JSON.stringified before comming here
    })
        .then(handleErrors)
        .then(res => Promise.resolve(res.json()))
        .catch(status => {
            console.log("Something happened : ", status)
            throw status;
        })
}

/* When using correct username password this function return a json file
 * with the jwt token in it.
 */
export async function authenticate(username, password) {
    return fetch(apiPath + "/login", {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        // body: JSON.stringify({
        //     username: username,
        //     password: password,
        // }),
        body: urlencode({
            username: username,
            password: password
        })
    }).then(res => {
        return Promise.resolve(res.json())
    })
}

export function urlencode(obj) {
    var formBody = []
    for (var property in obj) {
        var encodedKey = encodeURIComponent(property)
        var encodedValue = encodeURIComponent(obj[property])
        formBody.push(encodedKey + "=" + encodedValue)
    }
    return formBody.join("&")
}

/* Dynamically load a javascript "used by components" */
export function loadScript(url, callback) {
    let script = document.createElement('script')
    script.type = 'text/javascript'

    if (script.readyState) {
        //IE
        script.onreadystatechange = function() {
            if (
                script.readyState === 'loaded' ||
                script.readyState === 'complete'
            ) {
                script.onreadystatechange = null
                callback()
            }
        }
    } else {
        //Others
        script.onload = function() {
            callback()
        }
    }

    script.src = url
    document.getElementsByTagName('body')[0].appendChild(script)
}

/*
 * Should be bounded to the component before used
 * Changes the state of the component
 * used from forms when a form property is changed
 */
export function propertyChanged(event) {
    var tmp = {}

    /* update the variable with name as the id of the form */
    tmp[event.target.id] = event.target.value

    /* Update the state of the component */
    update_state.bind(this)(tmp)
}

export async function validateToken() {
    return fetch(apiPath + "/validateToken", {
        'headers': {
            'X-OBSERVATORY-AUTH': localStorage.getItem("accessToken")
        }
    })
    .then(res => res.status)
}

export async function userSignOut() {
    // Send request to logout in backend
    // and delete localStorage
    console.log("log out")
    await http_post_urlencoded("/logout", null)
    .then(json => {
        console.log("Got ", json)
        localStorage.removeItem("accessToken")
        window.location.href = "/login" // its needed because the hash is rerendering and we are asynchronous (localStorage does not trigger re-render of components)
    })
}

export function validateContext(context) {
    return !!context ? context : ""
}


export function createInfoWindowString(item) {
    var tableString =
    `<table class="table table-bordered table-hover">
        <tr>
            <td><strong> ID <strong></td>
            <td>` + item.id + ` </td>            
        </tr>
        <tr>
            <td><strong> Address <strong></td>
            <td> ` + item.address +` </td>            
        </tr>
    </table>`

    var contentString =
        '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1>' +
        item.name +
        '</h1>' +
        '<div id="bodyContent">' +
        tableString +
        '</div>' +
        '</div>'

        return contentString
}

export async function http_delete(endpoint) {

    // Force no trailing slash at the start of the endpoint
    if (endpoint.startsWith("/"))
        endpoint = endpoint.substring(1);

    return fetch(apiPath + "/" + endpoint, {
        // 'mode': 'cors', //maybe not needed
        'method': 'DELETE',
        'headers': {
            'X-OBSERVATORY-AUTH': localStorage.getItem("accessToken")
        }
    })
    .then(handleErrors)
    .then(res => Promise.resolve(res.json()))
    .catch(status => {
        console.log("Something happened : ", status)
        throw status;
    })
}