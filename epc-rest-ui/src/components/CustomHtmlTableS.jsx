import React, { Component } from 'react'
import '../css/custom.css'
import {Link} from 'react-router-dom'

class CustomHtmlTableS extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    processWithdrawn(item) {
        return item.withdrawn ? "fa fa-circle text-danger" : "fa fa-circle text-success"
    }

    render() {
        const { shops } = this.props
        return (
            <div className="box">
                <div className="box-header" />
                <div className="box-body">
                    <table className="table table-hover table-striped table-bordered">
                        <tbody>
                            <tr>
                                <th>Shop ID</th>
                                <th>Shop Name</th>
                                <th>Shop Latitude</th>
                                <th>Shop Longtitude</th>
                                <th>Shop Address </th>
                                <th>Shop Tags </th>
                                <th>Status</th>
                            </tr>

                            {shops.map((item, index) => (
                                <tr key={item.id}>
                                    <td><Link to={`/shops/${item.id}`}>{item.id}</Link></td>
                                    <td>{item.name}</td>
                                    <td>{item.lat}</td>
                                    <td>{item.lng}</td>
                                    <td>
                                        <Link to={`/adminMap/${item.id}`}>
                                            {item.address}&nbsp;&nbsp;
                                            <i className="fa fa-map-marker"></i>
                                        </Link>
                                    </td>
                                    <td>{item.tags.join(',')}</td>
                                    <td>
                                        <i className={this.processWithdrawn(item)}></i>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default CustomHtmlTableS
