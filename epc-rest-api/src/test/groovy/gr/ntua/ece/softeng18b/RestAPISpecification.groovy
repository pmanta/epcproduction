package gr.ntua.ece.softeng18b

import gr.ntua.ece.softeng18b.client.model.*

import gr.ntua.ece.softeng18b.client.RestAPI
import gr.ntua.ece.softeng18b.client.model.Product
import gr.ntua.ece.softeng18b.client.rest.RestCallFormat

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll

@Stepwise class RestAPISpecification extends Specification {

    private static final String IGNORED = System.setProperty("IGNORE_SSL_ERRORS", "true")
    
    private static final String HOST = "localhost"//System.getProperty("gretty.host")
    private static final String PORT = "8765"//System.getProperty("gretty.httpsPort")
	
    @Shared RestAPI api = new RestAPI(HOST, PORT as Integer, true)
    
    def "User logins"() {
        when:
        api.login("user", "aaaaa", RestCallFormat.JSON)
        
        then:
        api.isLoggedIn()
    }
    
    
        @Unroll
    def "post product "() {
        given:
        Product posted = new Product(
            name       : "Product",
            description: "Description",
            category   : "Category",
            tags       : ["x", "y", "z"],
        )
        Product returned = api.postProduct(posted, RestCallFormat.JSON)

        expect:
        returned.name == posted.name &&
        returned.description == posted.description
        returned.category == posted.category &&
        returned.tags.toSorted() == posted.tags.toSorted() &&
        !returned.withdrawn
    }
    
     @Unroll
    def "fetch products query after post"() {
        given:
        ProductList list = api.getProducts(
            0,
            20,
            "ALL",
            "id|DESC",
            RestCallFormat.JSON
        )
        println "Fetched product list $list"
		List<String> results = new ArrayList(); 
		results.add("Product");
        expect:
        list.start == 0 &&
        list.total == 1  
        list.products.collect { it.name } == results   
    }
    
    @Unroll
    def "put product"() {
        given:
        Product put = new Product(
   //     	id : "1",
            name       : "ProductPUT",
            description: "DescriptionPUT",
            category   : "CategoryPUT",
            tags       : ["xPUT", "yPUT", "zPUT"],
        )
        Product returned = api.putProduct("1",put, RestCallFormat.JSON)
   //     println "Put product $returned at index ${productIds.size()-1}"

        expect:
        returned.name == put.name &&
        returned.description == put.description
        returned.category == put.category &&
        returned.tags.toSorted() == put.tags.toSorted() &&
        returned.withdrawn == put.withdrawn

    }
    
      @Unroll
    def "fetch products query after put"() {
        given:
        ProductList list = api.getProducts(
            0,
            20,
            "ALL",
            "id|DESC",
            RestCallFormat.JSON
        )
        println "Fetched product list $list"
		List<String> results = new ArrayList(); 
		results.add("ProductPUT");
        expect:
        list.start == 0 &&
        list.total == 1  
        list.products.collect { it.name } == results   
    }
    
      @Unroll
    def "delete product user"() {
 when:
        api.deleteProduct("1", RestCallFormat.JSON)
        
        then:
        noExceptionThrown()   
         }
    
     @Unroll
    def "fetch products query after delete user"() {
        given:
        Product p = api.getProduct(
            "1",
            RestCallFormat.JSON
        )
        expect:
        p.withdrawn == true   
    }   
    
     def "logout"() {
        when:
        api.logout(RestCallFormat.JSON)

        then:
        !api.isLoggedIn()
    }
    
     def "Admin logins"() {
        when:
        api.login("admin", "aaaaa", RestCallFormat.JSON)
        
        then:
        api.isLoggedIn()
    }
    
     @Unroll
    def "delete product admin"() {
 when:
        api.deleteProduct("1", RestCallFormat.JSON)
        
        then:
        noExceptionThrown()   
         }
    
     @Unroll
    def "fetch products query after delete admin"() {
          given:
        ProductList list = api.getProducts(
            0,
            20,
            "ALL",
            "id|DESC",
            RestCallFormat.JSON
        )
  
        expect:
        list.total == 0 
    
    }
   
}

