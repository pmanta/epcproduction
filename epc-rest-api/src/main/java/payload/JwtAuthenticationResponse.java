package payload;

public class JwtAuthenticationResponse {
//    private String accessToken; // removed for testing the test of login
	private String token;
//    private String tokenType = "Bearer"; //removed for testing the test of login

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public JwtAuthenticationResponse(String accessToken) {
		this.token = accessToken;
	}
//    public JwtAuthenticationResponse(String accessToken) {
//        this.accessToken = accessToken;
//    }
//
//    public String getAccessToken() {
//        return accessToken;
//    }
//
//    public void setAccessToken(String accessToken) {
//        this.accessToken = accessToken;
//    }
//
//    public String getTokenType() {
//        return tokenType;
//    }
//
//    public void setTokenType(String tokenType) {
//        this.tokenType = tokenType;
//    }
}