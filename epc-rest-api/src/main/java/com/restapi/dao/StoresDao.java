package com.restapi.dao;

import java.util.List;
import java.util.Map;

import com.restapi.Store;

// Prices data access object
// Used to give an interface for accessing the stores data
public interface StoresDao {
	void insertStore(Store store);

	void insertStores(List<Store> stores);

	void updateStore(Store store);

	List<Store> getAllStores(String orderBy, String direction, String status);

	boolean storeExists(String address, String name);

	boolean storeExistsById(long id);

	Store getStoreById(long id);

	void deleteStoreAdmin(String id);

	void deleteStoreNoAdmin(String id);

	Store patchStore(Map<String, Object> updates, String id);
}
