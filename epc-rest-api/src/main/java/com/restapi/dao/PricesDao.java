package com.restapi.dao;

import com.restapi.Price;

import java.util.Date;
import java.util.List;

// Prices data access object
// Used to give an interface for accessing the prices data
// Here declare which methods can be used to access the data
public interface PricesDao {

	List<Price> getPrice(java.sql.Date dateFrom, java.sql.Date  dateTo, Double geoDist, Double geoLng, Double geoLat, List<String> shops, List<String> products, List<String> tags, List<String> sort);
    void setPrice(Double price,java.sql.Date dateFrom, java.sql.Date  dateTo,int productId,int shopId);
}
