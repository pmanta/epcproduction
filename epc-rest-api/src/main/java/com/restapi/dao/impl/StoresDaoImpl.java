package com.restapi.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.restapi.ShopHasProduct;
import com.restapi.Store;
import com.restapi.dao.StoresDao;

@Transactional
@Repository
public class StoresDaoImpl extends JdbcDaoSupport implements StoresDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public void insertStore(Store store) {
		String sql = "INSERT INTO stores (name,address,longitude,latitude,withdrawn,users_idUsers) values (?,?,?,?,?,1)";
		jdbcTemplate.update(sql, store.getName(), store.getAddress(), store.getLng(), store.getLat(),
				store.isWithdrawn());

		sql = "SELECT idStores FROM stores WHERE address = ? and name=?";
		Integer idStores = jdbcTemplate.queryForObject(sql, Integer.class, store.getAddress(), store.getName());
		System.out.println("GOING IN FOR");
		for (String tag : store.getTags()) {
			String sql2 = "INSERT INTO stores_tags (tag,stores_idStores,stores_users_idUsers) values (?,?,1)";
			jdbcTemplate.update(sql2, tag, idStores.toString());
		}
		store.setId(idStores.toString());
	}

	@Override
	public void insertStores(List<Store> Stores) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Store> getAllStores(String orderBy, String direction, String status) {
		System.out.println(orderBy + direction + status);
		String sql = "SELECT `s`.*, GROUP_CONCAT(`st`.`tag` SEPARATOR ',') AS `tags`  "
				+ "FROM stores `s` LEFT JOIN stores_tags `st`  "
				+ "ON s.idStores = st.stores_idStores AND st.stores_users_idUsers = s.users_idUsers "
				+ "WHERE withdrawn = " + status + " GROUP BY idStores,users_idUsers" + " ORDER BY " + orderBy + " "
				+ direction;

		List<Store> stores = new ArrayList<Store>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		for (Map<String, Object> row : rows) {
			Store store = new Store();
			store.setId((String) row.get("idStores").toString());
			store.setName((String) row.get("name"));

			store.setAddress((String) row.get("address"));

			store.setLng((double) row.get("longitude"));

			store.setLat((double) row.get("latitude"));

			store.setWithdrawn((boolean) row.get("withdrawn"));
			if (null != row.get("tags"))
				store.setTags((List<String>) Arrays.asList(row.get("tags").toString().split(",")));
			else
				store.setTags(new ArrayList<String>());

			stores.add(store);
		}

		return stores;
	}

	@Override
	public Store getStoreById(long id) {
		String sql = "SELECT `s`.*, GROUP_CONCAT(`st`.`tag` SEPARATOR ',') AS `tags` FROM stores `s` LEFT JOIN stores_tags `st` ON  s.idStores = st.stores_idStores AND st.stores_users_idUsers = s.users_idUsers WHERE s.idStores=? ";
		return (Store) jdbcTemplate.queryForObject(sql, new Object[] { id }, new RowMapper<Store>() {
			@Override
			public Store mapRow(ResultSet row, int rwNumber) throws SQLException {
				Store store = new Store();
				store.setId(row.getString("idStores").toString());
				store.setName(row.getString("name"));

				store.setAddress(row.getString("address"));

				store.setLng(row.getDouble("longitude"));

				store.setLat(row.getDouble("latitude"));

				store.setWithdrawn(row.getBoolean("withdrawn"));
				if (null != row.getString("tags"))
					store.setTags((List<String>) Arrays.asList(row.getString("tags").toString().split(",")));
				else
					store.setTags(new ArrayList<String>());
				return store;
			}
		});
	}

	@Override
	public boolean storeExists(String address, String name) {
		String sql = "SELECT count(*) FROM stores WHERE address = ? and name=?";
		int count = jdbcTemplate.queryForObject(sql, Integer.class, address, name);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean storeExistsById(long id) {
		String sql = "SELECT count(*) FROM stores WHERE idStores = ?";
		int count = jdbcTemplate.queryForObject(sql, Integer.class, id);
		if (count == 0) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void updateStore(Store store) {

		String sql = "SELECT * FROM stores_has_products WHERE stores_idStores = ?";
		List<ShopHasProduct> shps = new ArrayList<ShopHasProduct>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql, store.getId());
		for (Map<String, Object> row : rows) {
			ShopHasProduct shp = new ShopHasProduct();
			shp.setIdStores((String) row.get("stores_idStores").toString());
			shp.setIdProducts((String) row.get("products_idProducts").toString());
			shp.setPrice((String) row.get("price").toString());
			shp.setDate((String) row.get("Date").toString());
//			shp.setDateFrom((String) row.get("DateFrom").toString());
//			shp.setDateTo((String) row.get("DateTo").toString());
			shps.add(shp);
		}
		sql = "DELETE FROM `stores` WHERE `stores`.`idStores` = ? ";
		jdbcTemplate.update(sql, store.getId());

		String sql3 = "INSERT INTO stores (idStores,name,address,longitude,latitude,withdrawn,users_idUsers) values (?,?,?,?,?,?,1)";
		jdbcTemplate.update(sql3, store.getId(), store.getName(), store.getAddress(), store.getLng(), store.getLat(),
				store.isWithdrawn());
		
		// check if tags is null and throw bad request HERE!!!
		// added to check if tags are null
		for (String tag : store.getTags()) {
			String sql2 = "INSERT INTO stores_tags (tag,stores_idStores,stores_users_idUsers) values (?,?,1)";
			jdbcTemplate.update(sql2, tag, store.getId());
		}
		for (ShopHasProduct shpp : shps) {
			String sqlT = "INSERT INTO `stores_has_products`(`stores_idStores`, `products_idProducts`, "
					+ "`price`, `Date`) VALUES" + "(?,?,?,?)";
			jdbcTemplate.update(sqlT, shpp.getIdStores(),
								shpp.getIdProducts(), 
								shpp.getPrice(),
								shpp.getDate());

		}
	}

	@Override
	public void deleteStoreAdmin(String id) {
		String sql = "DELETE FROM `stores` WHERE `stores`.`idStores` = ? ";
		jdbcTemplate.update(sql, id);
	}

	@Override
	public void deleteStoreNoAdmin(String id) {
		String sql = "UPDATE `stores` SET `withdrawn` = TRUE WHERE `idStores` = ?";
		jdbcTemplate.update(sql, id);
	}

	@Override
	public Store patchStore(Map<String, Object> updates, String id) {
		/*
		 * 
		 * Leitourgei ena pedio opws leei stn ekfwnisi
		 * 
		 */
		Map<String, String> jsonToColMap = new HashMap<String, String>();
		jsonToColMap.put("name", "name");
		jsonToColMap.put("address", "address");
		jsonToColMap.put("lng", "longitude");
		jsonToColMap.put("lat", "latitude");
		jsonToColMap.put("tag", "tag");
		jsonToColMap.put("withdrawn", "withdrawn");
		String sql;
		if (updates.containsKey("tags")) {
			@SuppressWarnings("unchecked")
			List<String> tags = (List<String>) updates.get("tags");
			sql = "DELETE FROM `stores_tags` WHERE `stores_idStores` = ?";
			jdbcTemplate.update(sql, id);
			for (String tag : tags) {
				String sql2 = "INSERT INTO stores_tags (tag,stores_idStores,stores_users_idUsers) values (?,?,1)";
				jdbcTemplate.update(sql2, tag, id);
			}
		} else {
			for (String key : updates.keySet()) {
				sql = "UPDATE `stores` SET " + jsonToColMap.get(key) + " = ? WHERE `idStores` = ? ";
				jdbcTemplate.update(sql, updates.get(key), id);
			}
		}
		sql = "SELECT `s`.*, GROUP_CONCAT(`st`.`tag` SEPARATOR ',') AS `tags` FROM stores `s` LEFT JOIN stores_tags `st` ON  s.idStores = st.stores_idStores AND st.stores_users_idUsers = s.users_idUsers WHERE s.idStores=? ";
		return (Store) jdbcTemplate.queryForObject(sql, new Object[] { id }, new RowMapper<Store>() {
			@Override
			public Store mapRow(ResultSet row, int rwNumber) throws SQLException {
				Store store = new Store();
				store.setId(row.getString("idStores").toString());
				store.setName(row.getString("name"));

				store.setAddress(row.getString("address"));

				store.setLng(row.getDouble("longitude"));

				store.setLat(row.getDouble("latitude"));

				store.setWithdrawn(row.getBoolean("withdrawn"));
				if (null != row.getString("tags"))
					store.setTags((List<String>) Arrays.asList(row.getString("tags").toString().split(",")));
				else
					store.setTags(new ArrayList<String>());
				return store;
			}
		});
	}
}
