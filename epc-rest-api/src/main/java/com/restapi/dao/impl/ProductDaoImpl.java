package com.restapi.dao.impl;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.restapi.Product;
import com.restapi.dao.ProductDao;

@Repository
public class ProductDaoImpl extends JdbcDaoSupport implements ProductDao {
	
	@Autowired
	DataSource dataSource;
	
	@PostConstruct
	private void initialize()
	{
		setDataSource(dataSource);
	}

	@Override
	public List<Product> getAllProducts(String orderBy, String direction, String status) {
		String sql = "CALL GET_PRODUCTS_W_TAGS(?,?,?)";
		
		List<Product> products = new ArrayList<Product>();
		
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql, new Object[] {orderBy, direction, status});
		
		for (Map<String, Object> row : rows) {
			Product product = new Product();
			product.setId((int) row.get("id"));
			product.setName((String) row.get("name"));
			// maybe check for null here
			product.setDescription((String) row.get("description"));
			product.setCategory((String) row.get("category"));
			product.setWithdrawn((boolean) row.get("withdrawn"));
			
			if (null != row.get("tags"))
				product.setTags((List<String>) Arrays.asList(row.get("tags").toString().split(",")));
			else
				product.setTags(new ArrayList<String>());
			
			product.setOrigin((boolean) row.get("origin"));
			product.setPhoto((String) row.get("photo"));
			
			products.add(product);
		}
		return products;
	}
	
	@Override
	public Product getProductById(long id) {
		// TODO Auto-generated method stub
		
		String sql = "CALL GET_PRODUCT_BY_ID(?)";
		return (Product) getJdbcTemplate().queryForObject(sql, new Object[]{id}, new RowMapper<Product>() {
			@Override
			public Product mapRow(ResultSet rs, int rwNumber) throws SQLException {
				Product pr = new Product();
				pr.setId(rs.getInt("id"));
				pr.setName(rs.getString("name"));
				pr.setDescription(rs.getString("description"));
				pr.setCategory(rs.getString("category"));
				pr.setWithdrawn(rs.getBoolean("withdrawn"));
				pr.setOrigin(rs.getBoolean("origin"));
				
				if (rs.getString("tags") == null)
					pr.setTags(new ArrayList<>());
				else
					pr.setTags((List<String>) Arrays.asList(rs.getString("tags").toString().split(",")));
				
				return pr;
			}
		});
	}
	
	@Override
	public Product getProductByName(String name) {
		String sql = "SELECT * FROM PRODUCTS WHERE name=?";
		
		List<Product> res_products = getJdbcTemplate().query(sql, new Object[]{name}, new RowMapper<Product>() {
			@Override
			public Product mapRow(ResultSet rs, int rwNumber) throws SQLException {
				Product pr = new Product();
				pr.setId(rs.getInt("id"));
				pr.setName(rs.getString("name"));
				pr.setDescription(rs.getString("description"));
				pr.setCategory(rs.getString("category"));
				pr.setWithdrawn(rs.getBoolean("withdrawn"));
				pr.setOrigin(rs.getBoolean("origin"));
				return pr;
			}
		});
		
		if (res_products.isEmpty())
			return null;
		
		return res_products.get(0);
	}
	
	@Override
	public Product insertProduct(Product product) {
		// Insert the product
		String sql = "INSERT INTO PRODUCTS (name,description,category,withdrawn,origin,photo) values (?,?,?,?,?,?)";
		
		getJdbcTemplate().update(sql,
								product.getName(), 
                                product.getDescription(), 
                                product.getCategory(),
                                product.isWithdrawn(),
                                product.isOrigin(),
                                product.getPhoto());
		
		// Get the new added product from the database
		Product new_p = getProductByName(product.getName());
		insertProductTags(new_p.getId(), product.getTags());

		// Get it and return it
		return getProductById(new_p.getId());
	}
	
	@Override
	public Product updateProduct(long id, Product p)
	{
		String sql = "UPDATE PRODUCTS SET name=?, description=?, " + 
					 "category=?, withdrawn=?, origin=?, photo=? " +
					 "WHERE id=?";
		getJdbcTemplate().update(sql, 
								p.getName(), 
								p.getDescription(), 
								p.getCategory(), 
								p.isWithdrawn(), 
								p.isOrigin(),
								p.getPhoto(),
								id);
		
		// Delete the tags from the tag table
		// and insert the new ones!!
		sql = "DELETE FROM PRODUCTS_TAGS WHERE PRODUCTS_idProducts=?";
		getJdbcTemplate().update(sql, id);
		
		insertProductTags(id, p.getTags());
		
		return getProductById(id);
	}

	/* ADMIN USER */
	@Override
	public void deleteProduct(long id)
	{
		String sql = "DELETE FROM `products` WHERE `products`.`id` = ? ";
		getJdbcTemplate().update(sql, id);
	}
	
	/* VOLUNTEER */
	@Override
	public void withdrawProduct(long id)
	{
		// Set withdraw column to true no matter what
		patchProduct(id, "withdrawn", true);
	}
	
	// TODO: Need to add type checking to the function HERE
	@Override
	public Product patchProduct(long id, String attr, Object val)
	{
		// Decide what the object is
		// tags => List<String>
		// Origin, withdrawn => boolean
		// other => String
		String sql;
		
		if ("tags".equals(attr)) {
			//Type check
			ArrayList<String> list_val = (ArrayList<String>) val;
			
			// Delete the tags from the tag table
			// and insert the new ones!!
			sql = "DELETE FROM PRODUCTS_TAGS WHERE PRODUCTS_idProducts=?";
			getJdbcTemplate().update(sql, id);
			
			insertProductTags(id, list_val);
		}
		else if ("origin".equals(attr) || "withdrawn".equals(attr)) {
			boolean bool_val = (boolean) val;
			
			// SQL injection is secure because we check the existence of the attr before calling the function
			sql = "UPDATE PRODUCTS SET "+ attr +"=? WHERE id=?";
			
			getJdbcTemplate().update(sql, bool_val, id);
			
			return getProductById(id);
		}
		else {
			String str_val = (String) val;
			
			// SQL injection is secure because we check the existance of the attr before calling the function
			sql = "UPDATE PRODUCTS SET "+ attr +"=? WHERE id=?";
			
			getJdbcTemplate().update(sql, str_val, id);
		}
		
		return getProductById(id);
	}
	
	@Override
	public boolean productExists(String name) {
		return getProductByName(name) != null;
	}
	
	@Override
	public void insertProductTags(long id, List<String> tags)
	{
		// check if tags is null and throw bad request HERE!!!
		if (tags.size() > 0) {
			// Insert its tags (change the name of the columns in the product tags)
			String sql = "INSERT INTO PRODUCTS_TAGS (PRODUCTS_idProducts, PRODUCTS_TAGScol) values ";
			
			List<String> list = Collections.nCopies(tags.size(), "(" + id + ",?)");
			// Construct the sql query
			sql += String.join(",", list);
			
			Object[] array = tags.toArray(new Object[tags.size()]);
			getJdbcTemplate().update(sql, array);
		}
	}

	@Override
	public boolean productExists(long id) {
		Product check_existance = getProductById(id);
    	return check_existance.getId() == id;
	}
}
