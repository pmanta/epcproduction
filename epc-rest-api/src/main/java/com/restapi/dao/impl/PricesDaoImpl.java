package com.restapi.dao.impl;

import java.sql.Array;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.restapi.Price;
import com.restapi.dao.PricesDao;

@Repository
public class PricesDaoImpl extends JdbcDaoSupport implements PricesDao {

    @Autowired
    DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }


    @Override
    public List<Price> getPrice(java.sql.Date dateFrom, java.sql.Date dateTo, Double geoDist, Double geoLng, Double geoLat, List<String> shops, List<String> products, List<String> tags, List<String> sort) {
        Object[] params;
        String sql = " select  *\n" +
                " from stores_has_products as sthp \n" +
                " inner join stores on sthp.STORES_idStores = idStores \n" +
                " inner join products on PRODUCTS_idProducts = id \n" +
                " left join (select PRODUCTS_idProducts,group_concat(PRODUCTS_TAGScol) as tags from products_tags group by PRODUCTS_idProducts) as PTAGS on id =  PTAGS.PRODUCTS_idProducts\n" +
                " left join (select STORES_idSTORES,group_concat(tag) as tags from stores_tags group by STORES_idSTORES) as STAGS on idStores =  STAGS.STORES_idSTORES\n" ;


        String sqlDist = "inner join (select distinct idStores,st_distance_sphere(point(?,?),point(stores.latitude,stores.longitude)) as dist\n" +
                " from stores\n" +
                " ) as Distance on sthp.STORES_idStores = Distance.idStores \n";

        if (geoDist != 0.001 * 1000.0) {
            sql = sql + sqlDist + " where Date between ? and ? \n" + "and dist <= ?";
            params = new Object[]{geoLat, geoLng, dateFrom, dateTo, geoDist};
        } else {
            sql = sql + " where Date between ? and ? \n";
            params = new Object[]{dateFrom, dateTo};
        }

        if (shops.iterator().hasNext()) {
            sql = sql + " and stores.idStores in (" + StringUtils.join(shops, ',') + ")\n";
        }
        if (products.iterator().hasNext()) {
            sql = sql + " and products.id in (" + StringUtils.join(products, ',') + ")\n";
        }
        if (tags.iterator().hasNext()) {


            sql = sql + " and (id = any (select PRODUCTS_idProducts from products_tags where products_tags.PRODUCTS_TAGScol in (" +
                    StringUtils.join(
                            tags.stream()
                                    .map(i -> "\"" + i + "\"")
                                    .collect(Collectors.toList()), ',') + "))\n" +
                    " or   idStores = any (select STORES_idStores from stores_tags where stores_tags.tag in (" +
                    StringUtils.join(
                            tags.stream()
                                    .map(i -> "\"" + i + "\"")
                                    .collect(Collectors.toList()), ',') + ")))\n";
        }

        sql =  sql + " order by " + StringUtils.join(sort,',') + "\n";
//
        List<Price> result = jdbcTemplate.query(sql, params, new RowMapper<Price>() {
            @Override
            public Price mapRow(ResultSet rs, int rwNumber) throws SQLException {
                    Price pr = new Price();
                    pr.setDate(rs.getDate("Date"));
                    pr.setProductId(rs.getString("PRODUCTS_idProducts"));
                    pr.setPrice(rs.getDouble("price"));
                    pr.setShopId(rs.getString("STORES_idStores"));
                    pr.setShopName(rs.getString("name"));
                    pr.setShopAddress(rs.getString("address"));
                    pr.setProductName(rs.getString("products.name"));
                    if (rs.getString("ptags.tags") == null) {
                        pr.setProductTags(new ArrayList<>());
                    } else {
                        String[] ptags = rs.getString("ptags.tags").split(",");
                        pr.setProductTags(Arrays.asList(ptags));
                    }
                    if (rs.getString("stags.tags") == null) {
                        pr.setShopTags(new ArrayList<>());
                    } else {
                    	String[] stags = rs.getString("stags.tags").split(",");
                        pr.setShopTags(Arrays.asList(stags));
                    }

                    if (geoDist != 0.001 * 1000.0) {
                        pr.setShopDist(rs.getInt("dist"));
                    } else {
                        pr.setShopDist(-1);
                    }
                    return pr;
                }

        });

        if (result.isEmpty()) {
//            return null;
        	return new ArrayList<Price>();
        }

        return result;
    }


    @Override
    public void setPrice(Double price, Date dateFrom, Date dateTo, int productId, int shopId) {
        String sql = "INSERT INTO `mydb`.`stores_has_products` (`STORES_idStores`, `PRODUCTS_idProducts`, `price`, `Date`) VALUES (?, ?, ?, ?);\n";

        String delsql = "DELETE FROM `mydb`.`stores_has_products` WHERE (`STORES_idStores` = ?) and (`PRODUCTS_idProducts` = ?) and (`Date` between  ? and ?)";
        getJdbcTemplate().update(delsql,shopId,productId,dateFrom,dateTo);
        for (LocalDate date = dateFrom.toLocalDate(); !date.isAfter(dateTo.toLocalDate()); date = date.plusDays(1))
        {
            getJdbcTemplate().update(sql,shopId,productId,price,date);
        }



    }
}
