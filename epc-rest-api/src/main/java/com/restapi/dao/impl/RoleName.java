package com.restapi.dao.impl;

public enum RoleName {
	ROLE_USER, ROLE_ADMIN
}
