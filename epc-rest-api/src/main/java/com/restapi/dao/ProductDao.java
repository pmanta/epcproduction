package com.restapi.dao;

import java.util.List;

import com.restapi.Product;

public interface ProductDao 
{
	List<Product> getAllProducts(String orderBy, String direction, String status);
	Product getProductById(long id);
	Product getProductByName(String name);
	Product insertProduct(Product product);
	void insertProductTags(long id, List<String> tags);
	Product updateProduct(long id, Product product);
	void deleteProduct(long id);
	void withdrawProduct(long id);
	Product patchProduct(long id, String attr, Object val);
	boolean productExists(String name);
	boolean productExists(long id);
}