package com.restapi;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.restapi.dao.UserRepository;
import com.restapi.dao.impl.StoresDaoImpl;
import com.restapi.security.JwtAuthenticationFilter;
import com.restapi.security.JwtTokenProvider;

import io.jsonwebtoken.Jwts;

class DeleteResponse {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

class StoreResponse {
	private int start;
	private int count;
	private int total;
	private List<Store> shops;

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<Store> getShops() {
		return shops;
	}

	public void setShops(List<Store> shops) {
		this.shops = shops;
	}
}

@RestController
public class StoreController {

	@Autowired
	UserRepository userRepository;

	@Value("${app.jwtSecret}")
	private String jwtSecret;

	@Autowired
	JwtTokenProvider tokenProvider;

	@Autowired
	private StoresDaoImpl storeDao;

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping(path = "/shops")
	/*
	 * 
	 * AN DWSEI TYXAIO ONOMA PARAMETROU --> BAD REQUEST?
	 * 
	 */
	public ResponseEntity<StoreResponse> getStores(@RequestParam(value = "start", defaultValue = "0") String start,
			@RequestParam(value = "count", defaultValue = "20") String count,
			@RequestParam(value = "status", defaultValue = "ACTIVE") String status,
			@RequestParam(value = "sort", defaultValue = "id|DESC") String sort,
			@RequestParam(value = "format", defaultValue = "json") String format) {
		HashSet<String> available_sorts = new HashSet<String>();
		HashMap<String, String> dbMap = new HashMap<String, String>();
		dbMap.put("id", "`s`.`idStores`");
		dbMap.put("name", "`s`.`name`");
		dbMap.put("ALL", "true OR withdrawn = false");
		dbMap.put("WITHDRAWN", "true");
		dbMap.put("ACTIVE", "false");
		available_sorts.add("id|ASC");
		available_sorts.add("id|DESC");
		available_sorts.add("name|ASC");
		available_sorts.add("name|DESC");
		if (!"json".equals(format)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		if (!available_sorts.contains(sort)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		if (!"WITHDRAWN".equals(status) && !"ALL".equals(status) && !"ACTIVE".equals(status)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		StoreResponse res = new StoreResponse();
		int s = Integer.parseInt(start);
		int c = Integer.parseInt(count);
		res.setStart(s);
		res.setCount(c);
		String[] splitted = sort.split("\\|");

		List<Store> storeList = storeDao.getAllStores(dbMap.get(splitted[0]), splitted[1], dbMap.get(status));

		int t = storeList.size();
		/*
		 * if (s > t - 1 || s < 0) { return
		 * ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null); }
		 */
//		res.setTotal(t);
//		// BASIC ERROR HANDLING
//		if (t - 1 >= s + c) {
//			res.setShops(storeList.subList(s, s + c));
//		} else
//			res.setShops(storeList.subList(s, t));
//		
		// BASIC ERROR HANDLING
		if (t - 1 >= s + c) {
			res.setShops(storeList.subList(s, s + c));
			res.setTotal(c); // we return c objects
		} else {
			res.setShops(storeList.subList(s, t));
			res.setTotal(t-s); // we return table size - start objects
		}
		return ResponseEntity.ok(res);

	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping(path = "/shops/{id}")
	public ResponseEntity<Store> getStoreById(@PathVariable(value = "id") int id,
			@RequestParam(value = "format", defaultValue = "json") String format) {
		if (!"json".equals(format)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		if (!storeDao.storeExistsById(id))
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		else {
			Store ret_store = storeDao.getStoreById(id);
			return ResponseEntity.ok(ret_store);
		}
	}

	/*
	 * 
	 * KENO YPOXREWTIKO PEDIO --> BAD REQUEST !!
	 * 
	 */
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(path = "/shops", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<Store> addShop(@ModelAttribute Store store, UriComponentsBuilder builder,
			@RequestHeader(value = "X-OBSERVATORY-AUTH") String authorization) {
		boolean flag;

		if (storeDao.storeExists(store.getAddress(), store.getName())) {
			flag = false;
		} else {
			storeDao.insertStore(store);
			flag = true;
		}

		if (flag == false) {
			return new ResponseEntity<Store>(HttpStatus.CONFLICT);
		}
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(builder.path("/shops/{id}").buildAndExpand(store.getId()).toUri());
		return new ResponseEntity<Store>(store, HttpStatus.OK);
	}

	/*
	 * 
	 * ALLAZEI TO ID H PUT? LOGIKA OXI
	 * 
	 *//* Change to application url encoded value */
	@CrossOrigin(origins = "http://localhost:3000")
	@PutMapping(path = "/shops/{id}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<Store> updateShop(@PathVariable(value = "id") int id, @ModelAttribute Store store,
			@RequestHeader(value = "Content-Type") String uselessSwagger,
			@RequestHeader(value = "X-OBSERVATORY-AUTH") String authorization) {
		if (store.getId() == null) {
			store.setId(String.valueOf(id));
		}
		if (storeDao.storeExistsById(id) && Integer.parseInt(store.getId()) == id) {
			storeDao.updateStore(store);
			return ResponseEntity.ok(store);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/shops/{id}", method = RequestMethod.PATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Store> partialUpdateGeneric(@RequestBody Map<String, Object> updates,
			@PathVariable("id") String id) {
		if (storeDao.storeExistsById(Long.parseLong(id, 10))) {
			Store store = storeDao.patchStore(updates, id);
			return ResponseEntity.ok(store);
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@DeleteMapping(path = "/shops/{id}")
	public ResponseEntity<DeleteResponse> deleteShop(HttpServletRequest httpServletRequest,
			@PathVariable(value = "id") int id) {

		if (storeDao.storeExistsById(id)) {
			String token = httpServletRequest.getHeader(JwtAuthenticationFilter.JWT_TOKEN_HEADER_PARAM);
	//		String token = bearerToken.substring(7, bearerToken.length());
			Long idUsers = Long
					.parseLong(Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject());
			String role = userRepository.findRoleByid_users(idUsers).name();
			System.out.println(role);
			boolean isAdmin = role.equals("ROLE_ADMIN");
			DeleteResponse ds = new DeleteResponse();
			ds.setMessage("OK");
			if (isAdmin) {
				storeDao.deleteStoreAdmin(String.valueOf(id));
				return ResponseEntity.ok(ds);
			} else {
				storeDao.deleteStoreNoAdmin(String.valueOf(id));
				return ResponseEntity.ok(ds);
			}
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}
}