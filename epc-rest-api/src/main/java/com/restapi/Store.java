package com.restapi;

import java.util.List;

//Class describing the store entity in the table see Product.java for reference
public class Store {
	private String id;
	private String name;
	private String address;
	private double lng;
	private double lat;
	private List<String> tags;
	private boolean withdrawn;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public boolean isWithdrawn() {
		return withdrawn;
	}

	public void setWithdrawn(boolean withdrawn) {
		this.withdrawn = withdrawn;
	}

	@Override
	public String toString() {
		return "Store [id=" + id + ", name=" + name + ", adress=" + address + ", lng=" + lng + ", lat=" + lat + ", tags="
				+ tags + ", withdrawn=" + withdrawn + "]";
	}
}
