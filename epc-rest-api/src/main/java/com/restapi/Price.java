package com.restapi;

import java.util.List;
import java.util.Date;

// Class describing the price entry in the table see Product.java for reference
public class Price {

    private Double price;
    private java.sql.Date date;
    private String productName;
    private String productId;
    private List<String> productTags;
    private String shopId;
    private String shopName;
    private List<String> shopTags;
    private String shopAddress;
    private Integer shopDist;

    public Price() {
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(java.sql.Date date) {
        this.date = date;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<String> getProductTags() {
        return productTags;
    }

    public void setProductTags(List<String> productTags) {
        this.productTags = productTags;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public List<String> getShopTags() {
        return shopTags;
    }

    public void setShopTags(List<String> shopTags) {
        this.shopTags = shopTags;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public Integer getShopDist() {
        return shopDist;
    }

    public void setShopDist(Integer shopDist) {
        this.shopDist = shopDist;
    }
}
