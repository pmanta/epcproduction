package com.restapi;

import com.restapi.dao.impl.StoresDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.restapi.dao.impl.ProductDaoImpl;

import com.restapi.dao.impl.PricesDaoImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

class PriceResponse {
	private int start;
	private int count;
	private int total;

	private List<Price> prices;

	public PriceResponse() {
		super();
	}

	public List<Price> getPrices() {
		return prices;
	}
	public void setPrices(List<Price> prices) {
		this.prices = prices;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class PriceController {
	
	@Autowired
	private PricesDaoImpl priceDao;
	@Autowired
	private ProductDaoImpl productDao;
	@Autowired
	private StoresDaoImpl storesDao;
	
	@RequestMapping(method = RequestMethod.GET, value = "/prices")
	public ResponseEntity<PriceResponse> getPrices(@RequestParam(value="start", defaultValue="0") String start,
										   @RequestParam(value="count", defaultValue="20") String count,
										   @RequestParam(value="geoDist",defaultValue = "0.001") Double geoDist,
										   @RequestParam(value="geoLng",defaultValue = "0.001") Double geoLng,
										   @RequestParam(value="geoLat",defaultValue = "0.001") Double geoLat,
										   @RequestParam(value="dateFrom",required = false) java.sql.Date dateFrom,
										   @RequestParam(value="dateTo",required = false)   java.sql.Date dateTo,
										   @RequestParam(value="shops", required = false,defaultValue = "") List<String> shops,
										   @RequestParam(value="products", required = false, defaultValue = "") List<String> products,
										   @RequestParam(value="tags", required = false,defaultValue = "") List<String> tags,
										   @RequestParam(value="sort", defaultValue = "price|ASC") List<String> sort){



		List<Price> p;

		if (dateFrom == null && dateTo == null){
			// εγκυρότητα δεδομένων εισόδου γενικά
			// να αλλάξω τη βάση
			Date date = new Date();


			dateFrom = new java.sql.Date(date.getTime());
			dateTo =   new java.sql.Date(date.getTime());
			 System.out.println(dateFrom);
			 System.out.println(dateTo);
		}
		else if (dateFrom.toString().isEmpty() || dateTo.toString().isEmpty()){

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}







		List<String> available_sorts = new ArrayList<>();
		available_sorts.add("price|ASC");
		available_sorts.add("price|DESC");
		available_sorts.add("geoDist|ASC");
		available_sorts.add("geoDist|DESC");
		available_sorts.add("date|ASC");
		available_sorts.add("date|DESC");

		Iterator<String> iter;
		iter = sort.iterator();
		while (iter.hasNext()){

		if ((!available_sorts.contains(iter.next()))){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		}



		sort.replaceAll(i-> {
			String[] splitted = i.split("\\|");
			i = splitted[0] + " " + splitted[1];
			return i ;});

		sort.iterator().forEachRemaining(i->System.out.println(i));






		//Κριτήριο απόστασης
		if((geoDist==0.001 && geoLng ==0.001 && geoLat ==0.001)) {
			//Το κριτήριο δεν εφαρμόζεται

			 p = priceDao.getPrice(dateFrom,dateTo,geoDist*1000.0,geoLng,geoLat,shops,products,tags,sort);


//			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
		}
		else if (geoLat == 0.001 || geoLng == 0.001){
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
		}else{
			 p = priceDao.getPrice(dateFrom,dateTo,geoDist*1000.0,geoLng,geoLat,shops,products,tags,sort);


		}

		PriceResponse res = new PriceResponse();

		int s = Integer.parseInt(start);

		int c = Integer.parseInt(count);
		res.setStart(s);
		res.setCount(c);

		int t = p.size();
		
		// 98 -> 100 % EASY
		
		res.setTotal(t);
		// BASIC ERROR HANDLING
		if (t - 1 >= s + c) {
			res.setPrices(p.subList(s, s + c));
//			res.setTotal(c); // we return c objects
		} else {
			res.setPrices(p.subList(s, t));
//			res.setTotal(t-s); // we return table size - start objects
		}
		
		return ResponseEntity.ok(res);
	}

    @RequestMapping(method = RequestMethod.POST, value = "/prices", consumes= MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<PriceResponse > insertProduct(@RequestHeader(value = "X-OBSERVATORY-AUTH") String authorization,
    											 @RequestParam(value="price", defaultValue="") Double price,
                                                 @RequestParam(value="dateFrom", defaultValue="") java.sql.Date dateFrom,
												 @RequestParam(value="dateTo", defaultValue="") java.sql.Date dateTo,
                                                 @RequestParam(value="productId", defaultValue="0") String productId,
                                                 @RequestParam(value="shopId", defaultValue="0") String shopId)
    {



		if(productDao.productExists(Long.parseLong(productId)) && storesDao.storeExistsById(Long.parseLong(shopId))) {
			priceDao.setPrice(price,dateFrom,dateTo,Integer.parseInt(productId), Integer.parseInt(shopId));
			List<String> shops = new ArrayList<>();
			List<String> products = new ArrayList<>();
			List<String> sorting = new ArrayList<>();
			List<String> taglist = new ArrayList<>();
			sorting.add("price ASC");
			products.add(productId);
			shops.add(shopId);

			List<Price> p = priceDao.getPrice(dateFrom,dateTo,0.001 * 1000.0,0.001,0.001,shops,products,taglist,sorting);

			PriceResponse res = new PriceResponse();
	
			int s = 0;
	
			res.setStart(s);
	
			int t = (p.size() > 0) ? p.size() : 0;
			
			res.setTotal(t);
			res.setPrices(p.subList(s, t));
	
			return ResponseEntity.ok(res);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
    }

}
