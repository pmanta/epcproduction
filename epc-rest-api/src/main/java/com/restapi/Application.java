package com.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import com.restapi.security.RedisUtil;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		RedisUtil.INSTANCE.resetRedis();
		SpringApplication.run(Application.class, args);
	}
}