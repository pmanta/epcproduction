package com.restapi;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/////////////////////// ALLAGESSSSSSSSSSSSSSSSSSSSS

/*



*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.restapi.dao.UserRepository;
import com.restapi.dao.impl.RoleName;
import com.restapi.dao.impl.User;
import com.restapi.security.JwtAuthenticationFilter;
import com.restapi.security.JwtTokenProvider;

import payload.ApiResponse;
import payload.JwtAuthenticationResponse;
import payload.LoginRequest;
import payload.SignUpRequest;

import java.util.Arrays;

class LogoutResponse {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}


@RestController
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtTokenProvider tokenProvider;

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<?> authenticateUser(@Valid @RequestParam("username") String username,
			@RequestParam("password") String password) {// @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
//				new UsernamePasswordAuthenticationToken(loginRequest.getusername(), loginRequest.getPassword()));
//				new UsernamePasswordAuthenticationToken(loginRequest.get("username"), loginRequest.get("password")));
				new UsernamePasswordAuthenticationToken(username, password));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = tokenProvider.generateToken(authentication);
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
	}

	@SuppressWarnings("unused")
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(path = "/logout")
	public ResponseEntity<LogoutResponse> logout(/*HttpServletRequest httpServletRequest,*/
												 @RequestHeader(JwtAuthenticationFilter.JWT_TOKEN_HEADER_PARAM) String token) {
		LogoutResponse logoutResponse = new LogoutResponse();
//		String bearerToken = httpServletRequest.getHeader(JwtAuthenticationFilter.JWT_TOKEN_HEADER_PARAM);
//		String token;
		System.out.println(token);
//		if (StringUtils.hasText(token) && bearerToken.startsWith("Bearer ")) {
		if (StringUtils.hasText(token)) {
//			token = bearerToken.substring(7, bearerToken.length());
			tokenProvider.invalidateRelatedTokens(token);
			logoutResponse.setMessage("OK");
			System.out.println(token);
			return ResponseEntity.ok(logoutResponse);
		} else {
//			token = null;
//			System.out.println(token);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), signUpRequest.getPassword(),
				RoleName.ROLE_USER);
		/* .............................ALLAGESSSSSSSSS...............(/ */
		user.setPassword(passwordEncoder.encode(user.getPassword()));

		User result = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(result.getUserName()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping("/signup_admin")
	public ResponseEntity<?> registerAdmin(@Valid @RequestBody SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST);
		}

		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), signUpRequest.getPassword(),
				RoleName.ROLE_ADMIN);
		user.setPassword(passwordEncoder.encode(user.getPassword()));

		User result = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(result.getUserName()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "Admin registered successfully"));
	}

	// Endpoint to validate Token (Used by react to check if user is logged in and
	// able to access a privateRoute
	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping("/validateToken")
	public ResponseEntity<?> validate(@RequestHeader(value = "X-OBSERVATORY-AUTH") String auth) {
		return new ResponseEntity<>(HttpStatus.OK);
	}
}