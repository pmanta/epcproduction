package com.restapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.restapi.dao.UserRepository;
import com.restapi.dao.impl.ProductDaoImpl;
import com.restapi.security.JwtAuthenticationFilter;

import io.jsonwebtoken.Jwts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* Assistant class to hold the response body to return
 * Would be easier to use generics with "payload" as a key
 * to the response body not "products", "stores", "prices", .. */
class ProductResponse {
	private int start;
	private int count;
	private int total;

	private List<Product> products;

	public ProductResponse() {
		super();
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
};

class MessageResponse {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class ProductController {

	@Autowired
	UserRepository userRepository;

	@Value("${app.jwtSecret}")
	private String jwtSecret;

	@Autowired
	private ProductDaoImpl productDao;

	@RequestMapping(method = RequestMethod.GET, value = "/products")
	public ResponseEntity<ProductResponse> getAllProducts(
			@RequestParam(value = "start", defaultValue = "0") String start,
			@RequestParam(value = "count", defaultValue = "20") String count,
			@RequestParam(value = "sort", defaultValue = "id|DESC") String sort,
			@RequestParam(value = "format", defaultValue = "json") String format,
			@RequestParam(value = "status", defaultValue = "ACTIVE") String status) {

		List<String> available_sorts = new ArrayList<>();
		available_sorts.add("id|ASC");
		available_sorts.add("id|DESC");
		available_sorts.add("name|ASC");
		available_sorts.add("name|DESC");

		HashMap<String, String> available_status = new HashMap<>();
		available_status.put("ALL", "");
		available_status.put("ACTIVE", "0");
		available_status.put("WITHDRAWN", "1");

		if (!available_sorts.contains(sort) || !available_status.containsKey(status) || !"json".equals(format)) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}

		ProductResponse res = new ProductResponse();

		int s = Integer.parseInt(start);

		int c = Integer.parseInt(count);
		res.setStart(s);
		res.setCount(c);

		/* Order by = splitted[0] , direction = splitted[1] */
		String[] splitted = sort.split("\\|"); // escape the | char because we are in regex!!
		System.out.println(splitted[0] + " " + splitted[1]);
		List<Product> productList = productDao.getAllProducts(splitted[0], splitted[1], available_status.get(status));

		int t = productList.size();
		
		// Change to Total
		
		// BASIC ERROR HANDLING
		if (t - 1 >= s + c) {
			res.setProducts(productList.subList(s, s + c));
			res.setTotal(c); // we return c objects
		} else {
			res.setProducts(productList.subList(s, t));
			res.setTotal(t-s); // we return table size - start objects
		}

		return ResponseEntity.ok(res);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/products/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable(value = "id") int id,
			@RequestParam(value = "format", defaultValue = "json") String format) {
		if (!"json".equals(format))
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		// Keep it like this here to not run multiple queries in the database
		Product ret_product = productDao.getProductById(id);

		if (ret_product.getId() != id)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);

		return ResponseEntity.ok(ret_product);
	}

	// Need to change to application/x-www-urlencoded for the test to
	// success!!!!!!!!!!!!!!!!!!!!!!!!
	// PASSED THE TEST !!!!
	@RequestMapping(method = RequestMethod.POST, value = "/products", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<Product> insertProduct(/* @RequestBody Product json */
			@ModelAttribute Product json, @RequestHeader(value = "X-OBSERVATORY-AUTH") String authorization) {
		if (productDao.productExists(json.getName()))
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);

		Product ret = productDao.insertProduct(json);
		return ResponseEntity.ok(ret);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/products/{id}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<Product> updateProduct(@PathVariable(value = "id") int id, @ModelAttribute Product json,
			@RequestHeader(value = "Content-Type") String uselessSwagger,
			@RequestHeader(value = "X-OBSERVATORY-AUTH") String authorization) {
		// Change all the values for the given product
		if (!productDao.productExists(id))
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);

		// Add try catch and if a value is exception is thrown return bad request :P
		Product ret_product = productDao.updateProduct(id, json);
		return ResponseEntity.ok(ret_product);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/products/{id}")
	public ResponseEntity<MessageResponse> deleteProduct(@PathVariable(value = "id") int id,
			@RequestHeader(value = "X-OBSERVATORY-AUTH") String authorization) {
		if (!productDao.productExists(id))
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		else {
			Long idUsers = Long.parseLong(
					Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authorization).getBody().getSubject());
			String role = userRepository.findRoleByid_users(idUsers).name();
			System.out.println(role);
			boolean isAdmin = role.equals("ROLE_ADMIN");
			MessageResponse ret_mes = new MessageResponse();
			ret_mes.setMessage("OK");
			if (isAdmin) {
				productDao.deleteProduct(id);
				return ResponseEntity.ok(ret_mes);
			} else {
				productDao.withdrawProduct(id);
				return ResponseEntity.ok(ret_mes);
			}
		}
	}

	@RequestMapping(method = RequestMethod.PATCH, value = "/products/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> patchProduct(@PathVariable(value = "id") int id,
			@RequestBody Map<String, Object> json, @RequestHeader(value = "X-OBSERVATORY-AUTH") String authorization) {

		if (json.size() != 1) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}

		String attr = json.keySet().toArray(new String[1])[0];
		System.out.println(attr);
		Object val = json.get(attr);
		HashSet<String> available_attributes = new HashSet<>();
		available_attributes.add("name");
		available_attributes.add("description");
		available_attributes.add("category");
		available_attributes.add("withdrawn");
		available_attributes.add("origin");
		available_attributes.add("photo");
		available_attributes.add("tags");
		if (!available_attributes.contains(attr))
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		if (!productDao.productExists(id))
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);

		Product ret_prod = productDao.patchProduct(id, attr, val);

		return ResponseEntity.ok(ret_prod);
	}
}
