package com.restapi;

public class ShopHasProduct {
	public String idStores;
	public String idProducts;
	public String price;
	public String Date;

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getIdStores() {
		return idStores;
	}

	public void setIdStores(String idStores) {
		this.idStores = idStores;
	}

	public String getIdProducts() {
		return idProducts;
	}

	public void setIdProducts(String idProducts) {
		this.idProducts = idProducts;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	
}
